﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WiiTracker
    {
    /// <summary>
    /// Interaction logic for dgCapturingViewSets.xaml
    /// </summary>
    public partial class dgCapturingViewSets : Window
        {
        public dgCapturingViewSets()
            {
            InitializeComponent();
            this.Owner=App.Current.MainWindow;
            }

        private DispatcherTimer CT;
        MainWindow MW=null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
            {
            //DialogResult=false;
            bStop.Content="Stop";
            MW=(this.DataContext as MainWindow);
            CT = new DispatcherTimer();
            CT.Interval = new TimeSpan(0, 0, 0,0,Convert.ToInt32( MW._CaptureInterval));
            CT.Tick += DT_Tick;
            CT.Start();
            }

         private void DT_Tick(object sender, EventArgs e)
            {
            CalibrationViewset CS = new CalibrationViewset(MW._ActiveWiimotes);
            
            if (CS.IsValidShot) /// at least one is valid 
                {
                foreach (ViewsetCaptor VC in MW._CurrentViewsetCaptors)
                    {
                    bool AcceptViewset=true;
                    foreach (CameraInfo CI in VC._CaptorCameras)
                        {
                        if (!CS.ContainsWii(CI))
                            {
                            AcceptViewset=false;
                            break;
                            }                       
                        }
                   
                    if (AcceptViewset)
                        {
                        List<CameraView> SelectedViews=new List<CameraView>();
                        foreach (CameraInfo CI in VC._CaptorCameras)
                            {
                             foreach (CameraView CV in CS._CameraViews)
                                {
                                if (CV._Camera==CI)
                                    {
                                    SelectedViews.Add(CV);
                                    }                       
                                }                       
                            }
                       
                        CalibrationViewset NV=new CalibrationViewset(SelectedViews);
                        VC._RelatedViewsets.Add(NV);
                        VC.NofifyPropertyChanged("_ViewsetCount");
                        VC.NofifyPropertyChanged("_SD");
                        }
                    }
                //CalibrationViews++;
               // AutoViews.Add(CS);
               // this.CalibrationSnapshots.Add(CS);
                //tcViews.SelectedIndex = 1;
                //lbStates.SelectedItem = CS;
                //lbStates.ScrollIntoView(CS);
                //CS.ResetIntrinsicCalibration();
                }

            

           // if (CalibrationViews == TotalCalibrationViews) CT.Stop();
            }


        void CheckStatus()
            {
            if (CT.IsEnabled)
                bStop.Content="Stop";
                else
                bStop.Content="Start";
            }
        
         private void bStop_Click(object sender, RoutedEventArgs e)
             {
             if (bStop.Content.ToString()=="Stop")
                 CT.Stop();
                 else
                 CT.Start();
             CheckStatus();
             }

         private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
             {
             //this.Owner=null;
             CT.Stop();
             MW=null;
             }

         private void ExitButton_Click(object sender, RoutedEventArgs e)
             {
             Close();
             }

         private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
             {
             if (MW!=null)
                CT.Interval = new TimeSpan(0, 0, 0,0,Convert.ToInt32( MW._CaptureInterval));
             }

         private void Submit_Click(object sender, RoutedEventArgs e)
             {
             DialogResult=true;
             }
        }
    }
