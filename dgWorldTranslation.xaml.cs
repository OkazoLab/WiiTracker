﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using System.Linq;
using Emgu.CV;
using System.Windows.Media.Media3D;


namespace WiiTracker
    {
    /// <summary>
    /// Interaction logic for dgWorldTranslation.xaml
    /// </summary>
    public partial class dgWorldTranslation : Window, INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }
       
        public dgWorldTranslation()
            {
            InitializeComponent();
            DataContext=this;
            }

        private string LogA;

        public string _LogA
            {
            get { return LogA; }
            set { LogA = value; 
                    OnPropertyChanged("_LogA");
                }
            }

        private string LogB;

        public string _LogB
            {
            get { return LogB; }
            set { 
                LogB = value; 
                OnPropertyChanged("_LogB");
                }
            }

        

        private void OpenLogAClick(object sender, RoutedEventArgs e)
            {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse Log file for the current world";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Log files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == true)
                {
                _LogA = openFileDialog1.FileName;
                }
            }

        private void OpenLogBClick(object sender, RoutedEventArgs e)
            {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse Log file for the remote world";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Log files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == true)
                {
                _LogB = openFileDialog1.FileName;
                }
            }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
            {
            DataContext=null;
            }

         DataTable DataA,DataB;   

         private void EstimateTranslationClick(object sender, RoutedEventArgs e)
            {
            DataA=new DataTable();
            DataA.Columns.Add("Time",typeof(double));
            DataA.Columns.Add("X",typeof(double));
            DataA.Columns.Add("Y",typeof(double));
            DataA.Columns.Add("Z",typeof(double));

            DataB=new DataTable();
            DataB.Columns.Add("Time",typeof(double));
            DataB.Columns.Add("X",typeof(double));
            DataB.Columns.Add("Y",typeof(double));
            DataB.Columns.Add("Z",typeof(double));
            try
                {
                string[] records = File.ReadAllLines(LogA);
                int n=0;
                    foreach(string record in records)
                      {
                      DataRow r = DataA.NewRow();
                      
                      if (n==0)
                        {n=1;
                         continue;
                         }
                     
                      string[] fields = record.Split(',');
                      
                      r[0]=Convert.ToDouble(fields[0]); 

                       try{
                        r[1]=Convert.ToDouble(fields[2]);
                        r[2]=Convert.ToDouble(fields[3]);
                        r[3]=Convert.ToDouble(fields[4]);
                        }
                        catch
                            {
                            r[1]=Double.NaN;
                            r[2]=Double.NaN;
                            r[3]=Double.NaN;
                            }

                    
                      DataA.Rows.Add(r);
                      }

                records = File.ReadAllLines(LogB);
                n=0;
                    foreach(string record in records)
                    {
                    if (n==0)
                        {n=1;
                         continue;
                         }
                        
                      DataRow r = DataB.NewRow();
                      string[] fields = record.Split(',');
                      if (fields.Length<5)
                        {
                        }
                      r[0]=Convert.ToDouble(fields[0]); 
                      try{
                        r[1]=Convert.ToDouble(fields[2]);
                        r[2]=Convert.ToDouble(fields[3]);
                        r[3]=Convert.ToDouble(fields[4]);
                        }
                        catch
                            {
                            r[1]=Double.NaN;
                            r[2]=Double.NaN;
                            r[3]=Double.NaN;
                            }

                    
                      DataB.Rows.Add(r);
                    }

                /// Collect coordinate pairs
                /// 

                List<Emgu.CV.Structure.MCvPoint3D32f> ListA=new List<Emgu.CV.Structure.MCvPoint3D32f>();
                List<Emgu.CV.Structure.MCvPoint3D32f> ListB=new List<Emgu.CV.Structure.MCvPoint3D32f>();

                foreach (DataRow r in DataA.Rows)
                    { 
                    double Time=(double) r[0];
                   
                    if (Double.IsNaN(Time)) continue;
                    
                    var ClosestRecordB = DataB.Select().
                            OrderBy(dr => Math.Abs((double)dr[0] - (double) r[0])).//LastOrDefault();
                            FirstOrDefault();
                        
                     if (Math.Abs((double)ClosestRecordB[0] - (double) r[0])>100) continue; 
                     double XA=(double) r[1];
                     double YA=(double) r[2]; 
                     double ZA=(double) r[3];  

                     double XB=(double) ClosestRecordB[1];
                     double YB=(double) ClosestRecordB[2]; 
                     double ZB=(double) ClosestRecordB[3];

                      if (Double.IsNaN(XA)) continue;
                      if (Double.IsNaN(YA)) continue;
                      if (Double.IsNaN(ZA)) continue;

                      if (Double.IsNaN(XB)) continue;
                      if (Double.IsNaN(YB)) continue;
                      if (Double.IsNaN(ZB)) continue;

                      ListA.Add(new Emgu.CV.Structure.MCvPoint3D32f((float)XA,(float)YA,(float)ZA));
                      ListB.Add(new Emgu.CV.Structure.MCvPoint3D32f((float)XB,(float)YB,(float)ZB));
                    }
                 
                Emgu.CV.Structure.MCvPoint3D32f[] P1 = ListA.ToArray();
                Emgu.CV.Structure.MCvPoint3D32f[] P2 = ListB.ToArray();

                //P1 =new Emgu.CV.Structure.MCvPoint3D32f[300];
                //P2 =new Emgu.CV.Structure.MCvPoint3D32f[300];
                //for (int i=0;i<P1.Length;i++)
                //    {                    
                //    P1[i]=new Emgu.CV.Structure.MCvPoint3D32f(i,i,i);
                //    P2[i]=new Emgu.CV.Structure.MCvPoint3D32f(i+5,i+5,i+5);
                //    }

                //P1[0]=new Emgu.CV.Structure.MCvPoint3D32f(0,0,0);
                //P2[0]=new Emgu.CV.Structure.MCvPoint3D32f(0,0,0);

                //P1[1]=new Emgu.CV.Structure.MCvPoint3D32f(1,0,0);
                //P2[1]=new Emgu.CV.Structure.MCvPoint3D32f(1,0,0);

                //P1[2]=new Emgu.CV.Structure.MCvPoint3D32f(-1,0,0);
                //P2[2]=new Emgu.CV.Structure.MCvPoint3D32f(-1,0,0);


                Matrix<double> MD = null;
                
                byte[] BT = null;               
                Emgu.CV.CvInvoke.CvEstimateAffine3D(P1, P2, out MD, out BT, 3, 0.99999);
                Vector3D V=Helper3D.GetEulerAngles(ResultMatrix);
                ResultMatrix = Helper3D.BuildTransformationMatrix(MD);
                V=Helper3D.GetEulerAngles(ResultMatrix);
                
                }
                catch{}
            }
        public  Matrix3D ResultMatrix=Matrix3D.Identity;

        private void bOk_Click(object sender, RoutedEventArgs e)
            {
            this.DialogResult=true;
            Close();
            }

        }
    }