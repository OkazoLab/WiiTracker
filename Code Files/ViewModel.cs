﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a GUI view for the application model 
// </copyright>
      
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteLib;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Emgu.CV;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Reflection;
using System.Windows.Markup;
using System.Windows;
using Fluent;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Controls;

namespace WiiTracker {

    public partial class MainWindow :  MetroWindow 
    {
    #region Application Constants

    int TargetNumber=4;


    #endregion

   
     
     
    #region Visualization Options

    bool ShowProjectionRays = true;
    public bool _ShowProjectionRays
        {
        get { return ShowProjectionRays; }
        set { ShowProjectionRays = value; }
        }



    bool Show2DTargetProjections = true;
    public bool _Show2DTargetProjections
        {
        get { return Show2DTargetProjections; }
        set { Show2DTargetProjections = value; }
        }

    bool ShowBaseTokenView = true;
    public bool _ShowBaseTokenView
        {
        get { return ShowBaseTokenView; }
        set {             
            ShowBaseTokenView = value; 
            ShowCameras(false);
            }
        }

    bool ShowRayInterSections = true;
    public bool _ShowRayInterSections
        {
        get { return ShowRayInterSections; }
        set { ShowRayInterSections = value; }
        }

     #endregion  
     
     
    #region Reconstruction

    private AsyncObservableCollection<CameraView> SelectedCameraViews = new AsyncObservableCollection<CameraView>();

        public AsyncObservableCollection<CameraView> _SelectedCameraViews
            {
            get { return SelectedCameraViews; }
            }

        private void RebuildSelectedCameraViews()
            {
            SelectedCameraViews.Clear();
            CameraInfo WI = cbRootWiimote.SelectedItem as CameraInfo;
            if (WI == null) return;
            foreach (CalibrationViewset CS in CalibrationSnapshots)
                {
                foreach (CameraView CV in CS._CameraViews)
                    {
                    if (CV._Camera == WI)
                        SelectedCameraViews.Add(CV);
                    }
                }
            }

        private void cbRootWiimote_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
            RebuildSelectedCameraViews();
            OnPropertyChanged("_SelectedCameraViews");
            }

    #endregion
   
     
     public string _Introduction
        {
        get            
            {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "WiiTracker.Resources.Introduction.rtf";
            string result ="";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
                {
                result = reader.ReadToEnd();
                }
            return result;
            }
        }


     int IRSensitivity = 0;

     public int _IRSensitivity
         {
         get { return IRSensitivity; }
         set { 
            IRSensitivity = value; 
            OnPropertyChanged("_IRSensitivity");
            }
         }

    
    #region Tracking Parameters and Results


    double TrackingRate = 0;
    public double _TrackingRate
        {
        get { return TrackingRate; }
        set { 
            TrackingRate = value; 
            OnPropertyChanged("_TrackingRate");
            }
        }

    double DistortionWeight = 0;
    public double _DistortionWeight
        {
        get { return DistortionWeight; }
        set { 
             DistortionWeight = value; 
            OnPropertyChanged("_DistortionWeight");
        }
        }

    string LogFolder =System.AppDomain.CurrentDomain.BaseDirectory;



    double PoseWeight = 0;
    public double _PoseWeight
        {
        get { return PoseWeight; }
        set { PoseWeight = value; 
        OnPropertyChanged("_PoseWeight");
        }
        }


    double BalanceWeight = 0.5;
    public double _BalanceWeight
        {
        get { return BalanceWeight; }
        set { BalanceWeight = value; 
        OnPropertyChanged("_BalanceWeight");
        }
        }


    #region Coordinates of tracked targets

    List<bool> IsTargetTracked = new List<bool>(){true,true,true,true} ;

    public List<bool> _IsTargetTracked
        {
        get { return IsTargetTracked; }
        }

    List<TrackingResult> TrackedTargets = new List<TrackingResult>() { new TrackingResult(), new TrackingResult(), new TrackingResult(), new TrackingResult() };

    public List<TrackingResult> _TrackedTargets
        {
        get { return TrackedTargets; }
        }

        #endregion


        #endregion

     int LPTPortNumber = 888;
     public int _LPTPort
        {
        get { return LPTPortNumber; }
        set {
            LPTPortNumber = value; 
            OnPropertyChanged("_LPTPort");
            }
        }

     int TriggerValue = 255;
     public int _TriggerValue
        {
        get { return TriggerValue; }
        set {
            TriggerValue = value; 
            OnPropertyChanged("_TriggerValue");
            }
        }

     int RecordingTime = 0;
     public int _RecordingTime
        {
        get { return RecordingTime; }
        set {
            RecordingTime = value; 
            OnPropertyChanged("_RecordingTime");
            }
        }
    }    
}
