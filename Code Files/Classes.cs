﻿//<copyright
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a list of classes for the application model
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using System.Xml.Serialization;
using Emgu.CV;
using WiimoteLib;

namespace WiiTracker
    {
    public class ViewsetCaptor : INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        public void NofifyPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }

        public ViewsetCaptor(List<CameraInfo> CaptorCameras)
            {
            this.CaptorCameras = CaptorCameras;
            }

        private List<CameraInfo> CaptorCameras;

        public List<CameraInfo> _CaptorCameras
            {
            get { return CaptorCameras; }
            }

        private AsyncObservableCollection<CalibrationViewset> RelatedViewsets = new AsyncObservableCollection<CalibrationViewset>();

        public AsyncObservableCollection<CalibrationViewset> _RelatedViewsets
            {
            get { return RelatedViewsets; }
            set { RelatedViewsets = value; }
            }

        public int _ViewsetCount
            {
            get { return RelatedViewsets.Count; }
            }

        private bool IsAccepted = false;

        public bool _IsAccepted
            {
            get { return IsAccepted; }
            set { IsAccepted = value; }
            }

        private int AcceptedCount = 25;

        public int _AcceptedCount
            {
            get { 
                double S=Math.Sqrt(AcceptedCount);
                int s=(int)Math.Floor(S);
                AcceptedCount=s*s;
                return AcceptedCount; 
                }
            set { AcceptedCount = value; }
            }

        public double GetCameraMean(int index)
            {
            double OMean = 0;
            foreach (CalibrationViewset C in this._RelatedViewsets)
                {
                CameraView CV = C._CameraViews[index];
                OMean = OMean + CV.GetTargetPolygonArea();
                }
            if (this._RelatedViewsets.Count > 0)
                OMean = OMean / this._RelatedViewsets.Count;
            return OMean;
            }

        public double _SD
            {
            get
                {
                double CaptorSD = 0;
                for (int i = 0; i < CaptorCameras.Count; i++)
                    {
                    double MeanArea = 0;
                    foreach (CalibrationViewset C in this._RelatedViewsets)
                        {
                        CameraView CV = C._CameraViews[i];
                        MeanArea = MeanArea + Math.Sqrt(Math.Pow(CV._Quaternion[0],2)+Math.Pow(CV._Quaternion[1],2)+Math.Pow(CV._Quaternion[2],2));
                        // CV.GetTargetPolygonArea();
                        }
                    if (this._RelatedViewsets.Count > 0)
                        MeanArea = MeanArea / this._RelatedViewsets.Count;
                    double SD = 0;
                    foreach (CalibrationViewset C in this._RelatedViewsets)
                        {
                        CameraView CV = C._CameraViews[i];
                        SD = SD + Math.Pow(MeanArea - Math.Sqrt(Math.Pow(CV._Quaternion[0],2)+Math.Pow(CV._Quaternion[1],2)+Math.Pow(CV._Quaternion[2],2)), 2);
                        }
                    SD = Math.Sqrt(SD);
                    CaptorSD = CaptorSD + SD;
                    }
                if (CaptorCameras.Count > 0)
                    CaptorSD = CaptorSD / CaptorCameras.Count;
                return CaptorSD;
                }
            }
        }

    public class AsyncObservableCollection<T> : ObservableCollection<T>
        {
        private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

        public AsyncObservableCollection()
            {
            }

        public AsyncObservableCollection(IEnumerable<T> list)
            : base(list)
            {
            }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
            if (SynchronizationContext.Current == _synchronizationContext)
                {
                // Execute the CollectionChanged event on the current thread
                RaiseCollectionChanged(e);
                }
            else
                {
                // Post the CollectionChanged event on the creator thread
                _synchronizationContext.Post(RaiseCollectionChanged, e);
                }
            }

        private void RaiseCollectionChanged(object param)
            {
            // We are in the creator thread, call the base implementation directly
            base.OnCollectionChanged((NotifyCollectionChangedEventArgs)param);
            }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
            {
            if (SynchronizationContext.Current == _synchronizationContext)
                {
                // Execute the PropertyChanged event on the current thread
                RaisePropertyChanged(e);
                }
            else
                {
                // Post the PropertyChanged event on the creator thread
                _synchronizationContext.Post(RaisePropertyChanged, e);
                }
            }

        private void RaisePropertyChanged(object param)
            {
            // We are in the creator thread, call the base implementation directly
            base.OnPropertyChanged((PropertyChangedEventArgs)param);
            }
        }

    [ContentProperty("Name")]
    public class Reference : System.Windows.Markup.Reference
        {
        public Reference()
            : base()
            {
            }

        public Reference(string name)
            : base(name)
            {
            }

        public override object ProvideValue(IServiceProvider serviceProvider)
            {
            if (serviceProvider == null)
                {
                throw new ArgumentNullException();
                }
            IProvideValueTarget valueTargetProvider = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            if (valueTargetProvider != null)
                {
                DependencyObject targetObject = valueTargetProvider.TargetObject as DependencyObject;
                if (targetObject != null && DesignerProperties.GetIsInDesignMode(targetObject))
                    {
                    return null;
                    }
                }
            return base.ProvideValue(serviceProvider);
            }
        }

    public static class BrushSet
        {
        private static List<Color> ColorSet = new List<Color>(){
        Colors.Black,
        Colors.Red,
        Colors.Green,
        Colors.Blue,

        Colors.Orange,
        Colors.Magenta,
        Colors.Cyan,
        Colors.Yellow
        };

        private static SolidColorBrush[] FixedBrushes = new SolidColorBrush[8];

        public static void RecreateColorSet()
            {
            ColorSet = new List<Color>(){
                                        Colors.Red,
                                        Colors.Green,
                                        Colors.Blue,

                                        Colors.Orange,
                                        Colors.Magenta,
                                        Colors.Cyan,
                                        Colors.Yellow,
                                        Colors.Gray,
                                        Colors.RosyBrown,
                                        Colors.YellowGreen,
                                        Colors.DarkGoldenrod
                                        };
            }

        static BrushSet()
            {
            RecreateColorSet();
            for (int i = 0; i < FixedBrushes.Length; i++)
                {
                FixedBrushes[i] = new SolidColorBrush(ColorSet[i]);
                FixedBrushes[i].Freeze();
                }
            }

        public static Color GetNextColor()
            {
            Color C = ColorSet[0];
            ColorSet.Remove(C);
            return C;
            }

        public static void ReserveColor(Color C)
            {
            ColorSet.Remove(C);
            }

        public static SolidColorBrush GetBrush(int index)
            {
            if (index < FixedBrushes.Length)
                return FixedBrushes[index];
            else
                return PickBrush();
            }

        private static SolidColorBrush PickBrush()
            {
            SolidColorBrush result = Brushes.Transparent;

            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (SolidColorBrush)properties[random].GetValue(null, null);

            return result;
            }
        }

    public class StereoCorrespondence : INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }

        public StereoCorrespondence()
            {
            }

        public StereoCorrespondence(CameraInfo Camera1, CameraInfo Camera2, string Camera1ID, string Camera2ID, ExtrinsicCameraParameters EP, Matrix<double> Q, int ViewNumber, double Error)
            {
            this.Camera1ID = Camera1ID;
            this.Camera2ID = Camera2ID;
            this.Camera1 = Camera1;
            this.Camera2 = Camera2;

            //RestoreReferencies();
            this.EP = EP;
            this.Q = Q;
            this.ViewNumber = ViewNumber;
            this.CorrespondenceError = Error;
            }

        public string Camera1ID;
        public string Camera2ID;

        [XmlIgnore]
        public CameraInfo Camera1;

        [XmlIgnore]
        public CameraInfo Camera2;

        public void RestoreReferencies()
            {
            foreach (CameraInfo CI in (App.Current.MainWindow as MainWindow)._AvailableWiimotes)
                {
                if (CI._Index == Camera1ID)
                    this.Camera1 = CI;
                if (CI._Index == Camera2ID)
                    this.Camera2 = CI;
                }
            }

        //public CameraInfo Camera1
        //    {
        //    get
        //        {
        //        CameraInfo CI = null;
        //        Application.Current.Dispatcher.Invoke(new Action(() =>
        //        CI = (App.Current.MainWindow as MainWindow)._AvailableWiimotes[Camera1ID - 1]));
        //        return CI;
        //        }
        //    }

        //public CameraInfo Camera2
        //    {
        //    get
        //        {
        //        CameraInfo CI = null;
        //        Application.Current.Dispatcher.Invoke(new Action(() =>
        //        CI = (App.Current.MainWindow as MainWindow)._AvailableWiimotes[Camera2ID - 1]));
        //        return CI;
        //        }
        //    }

        public ExtrinsicCameraParameters EP;

        public override string ToString()
            {
            if (EP == null) return "test";
            //Vector3D V=Helper3D.EstimateEulerRotationAngless( GetStereoTransformMatrix());
            Matrix<double> TV = EP.TranslationVector;
            try
                {
                return
                Camera1._Name
                + " MSE:" + Camera1._MSE.ToString("#.00") + " x " + Camera2._Name + " MSE:" + Camera2._MSE.ToString("#.00")
                + " TV={" + TV[0, 0].ToString("0.00") + "," + (-TV[1, 0]).ToString("0.00") + "," + TV[2, 0].ToString("0.00") + "} "
                + "xPitch " + _TransformEulerAngles.X.ToString("0.00") + "  | yYaw " + _TransformEulerAngles.Y.ToString("0.00") + "  | zRoll " + _TransformEulerAngles.Z.ToString("0.00") +
                " | Error: " + CorrespondenceError.ToString("#.00") + " | Views: " + ViewNumber
                ;
                }
            catch
                {
                return "";
                }
            }

        private int ViewNumber = 0;

        public int _CalibrationShotNumber
            {
            get { return ViewNumber; }
            set { ViewNumber = value; }
            }

        public Matrix3D GetStereoTransformMatrix()
            {
            return Helper3D.BuildTransformationMatrix(EP.RotationVector, EP.TranslationVector);
            }

        /// disparity matrix
        public Matrix<double> Q;

        private double CorrespondenceError = 0;

        public double _CorrespondenceError
            {
            get { return CorrespondenceError; }
            set
                {
                CorrespondenceError = value;
                OnPropertyChanged("_CalibrationError");
                }
            }

        public Vector3D _TransformEulerAngles
            {
            get
                {
                return new Vector3D(EP.RotationVector[0, 0] * 180 / Math.PI, EP.RotationVector[1, 0] * 180 / Math.PI, EP.RotationVector[2, 0] * 180 / Math.PI);
                }
            }

        public string _Result
            {
            get
                {
                if (EP == null) return "Not calibrated";
                string S = "";
                S = S + "T= " + EP.TranslationVector[0, 0].ToString("0.00") + ", " + EP.TranslationVector[1, 0].ToString("0.00") + ", " + EP.TranslationVector[2, 0].ToString("0.00");
                S = S + "\nR= " + "xPitch " + _TransformEulerAngles.X.ToString("0.00") + "  | yYaw " + _TransformEulerAngles.Y.ToString("0.00") + "  | zRoll " + _TransformEulerAngles.Z.ToString("0.00");
                return S;
                }
            }
        }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    ///

    public class CameraInfo : INotifyPropertyChanged, ICloneable
        {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        public void OnPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }

        public CameraInfo()
            {
            }

        public CameraInfo(Wiimote Device, string Index)
            {
            this.Index = Index;
            this.Device = Device;
            }

        public double[] NormalizedTargetProximity = new double[4];
        private double GlobalPoseError = 0.0;

        public double _GlobalPoseError
            {
            get { return GlobalPoseError; }
            set
                {
                GlobalPoseError = value;
                OnPropertyChanged("_GlobalPoseError");
                }
            }

        public List<Matrix3D> TransformMatrices = new List<Matrix3D>();
        public List<double> PoseErrors = new List<double>();

        public MatrixTransform3D GlobalPoseTransform;

        public MatrixTransform3D _GlobalPoseTransform
            {
            get
                {
                return GlobalPoseTransform;
                }
            }

        private string Index = "";

        public string _Index
            {
            get { return Index; }
            set
                {
                Index = value;
                }
            }

        #region Intrinsic Calibration Info

        private double MSE = 0.0;

        public double _MSE
            {
            get { return MSE; }
            set
                {
                MSE = value;
                OnPropertyChanged("_MSE");
                OnPropertyChanged("_Tvec");
                OnPropertyChanged("_Rvec");
                }
            }

        public string _Tvec
            {
            get
                {
                if (ECP == null)
                    return "None";
                else
                    return "" + ECP[0].TranslationVector[0, 0].ToString("0.00") + " | " + ECP[0].TranslationVector[1, 0].ToString("0.00") + " | " + ECP[0].TranslationVector[2, 0].ToString("0.00");
                }
            }

        public Vector3D _TransformEulerAngles
            {
            get
                {
                if (ECP == null)
                    return new Vector3D(0, 0, 0);
                else
                    return new Vector3D(ECP[0].RotationVector[0, 0] * 180 / Math.PI, ECP[0].RotationVector[1, 0] * 180 / Math.PI, ECP[0].RotationVector[2, 0] * 180 / Math.PI);
                }
            }

        public string _Rvec
            {
            get
                {
                if (ECP == null)
                    return "None";
                else
                    return "" + _TransformEulerAngles.X.ToString("0.00") + " | " + _TransformEulerAngles.Y.ToString("0.00") + " | " + _TransformEulerAngles.Z.ToString("0.00");
                }
            }

        private bool IsIntrinsicCalibrated = false;

        public bool _IsIntrinsicCalibrated
            {
            get { return IsIntrinsicCalibrated; }
            set
                {
                IsIntrinsicCalibrated = value;
                OnPropertyChanged("_IsIntrinsicCalibrated");
                OnPropertyChanged("_CalibrationResult");
                }
            }

        #endregion Intrinsic Calibration Info

        public string _Name
            {
            get
                {
                if (Index.Length > 3)
                    return "Camera " + Index.Substring(0, 3);
                else
                    return "Camera -1";
                }
            }

        [NonSerialized]
        private Brush CameraBrush;

        private Color CameraBrushColor;

        public Brush _CameraBrush
            {
            get
                {
                if (CameraBrush == null)
                    {
                    if (CameraBrushColor.A == 0)
                        CameraBrushColor = BrushSet.GetNextColor();
                    else
                        BrushSet.ReserveColor(CameraBrushColor);
                    CameraBrush = new SolidColorBrush(CameraBrushColor);
                    CameraBrush.Freeze();
                    }

                return CameraBrush;//.GetBrush(Index);
                }
            }

        [NonSerialized]
        public bool IsConnected = false;

        public bool _IsConnected
            {
            get { return IsConnected; }
            set
                {
                //return;
                if (Device == null)
                    {
                    IsConnected = false;
                    return;
                    }
                if (value != IsConnected)
                    {
                    IsConnected = value;
                    if (IsConnected)
                        {
                        IsConnected = Connect();
                        //IsConnected =false;
                        }
                    else
                        Close();
                    OnPropertyChanged("_IsConnected");
                    }
                }
            }

        [NonSerialized]
        private Stopwatch SWatch = new Stopwatch();


        #region Mutual View
        double Opacity = 0.5;

        public double _Opacity
            {
            get { return Opacity; }
            set { 
                Opacity = value; 
                OnPropertyChanged("_Opacity");
                }
            }

        [NonSerialized]
        bool IsXReversed = false;

        public bool _IsXReversed
            {
            get { return IsXReversed; }
            set { IsXReversed = value; }
            }

        [NonSerialized]
        bool IsYReversed = false;

        public bool _IsYReversed
            {
            get { return IsYReversed; }
            set { IsYReversed = value; }
            }
        #endregion

        public System.Drawing.PointF[] GetCurrentTarget2DPositions()
            {
            System.Drawing.PointF[] P = new System.Drawing.PointF[4];
            if (Device == null) return P;
            for (int i = 0; i < 4; i++)
                {
                P[i].X = (Device.WiimoteState.IRState.IRSensors[i].Position.X) * 1024;
                P[i].Y = (1 - Device.WiimoteState.IRState.IRSensors[i].Position.Y) * 768;
                if ((P[i].X > 1023) || (P[i].X < 1) || (P[i].Y > 767) || (P[i].Y < 1))
                    P[i] = new System.Drawing.PointF((float)Double.MinValue, (float)Double.MinValue);
                    else
                    {
                    if (IsXReversed)
                        P[i].X=1024-P[i].X;
                    if (IsYReversed)
                        P[i].Y=768-P[i].Y;
                    }                   
                }

            

            return P;
            }

        public Point3D[] _TargetPositions3D
            {
            get
                {
                Point3D[] TP = new Point3D[4];
                for (int i = 0; i < 4; i++)
                    {
                    if (Device == null) TP[i] = new Point3D(0, 0, 0);
                    if ((Device.WiimoteState.IRState.IRSensors[i].Position.X * 1024 > 1023) || (Device.WiimoteState.IRState.IRSensors[i].Position.Y * 768 > 767))
                        TP[i] = new Point3D(double.MinValue, double.MinValue, Device.WiimoteState.IRState.IRSensors[i].Size);
                    else
                        TP[i] = new Point3D((Device.WiimoteState.IRState.IRSensors[i].Position.X) * 1024,
                                                        (1 - Device.WiimoteState.IRState.IRSensors[i].Position.Y) * 768, Device.WiimoteState.IRState.IRSensors[i].Size);
                    
                    if (IsXReversed)
                        TP[i].X=1024-TP[i].X;
                    if (IsYReversed)
                        TP[i].Y=768-TP[i].Y;
                    }
                return TP;
                }
            }

        public string[] _TargetLabels
            {
            get
                {
                Point3D[] TP = new Point3D[4];
                string[] Results=new string[4];
                for (int i = 0; i < 4; i++)
                    {
                    if (Device == null) TP[i] = new Point3D(0, 0, 0);
                    if ((Device.WiimoteState.IRState.IRSensors[i].Position.X * 1024 > 1023) || (Device.WiimoteState.IRState.IRSensors[i].Position.Y * 768 > 767))
                        TP[i] = new Point3D(double.MinValue, double.MinValue, Device.WiimoteState.IRState.IRSensors[i].Size);
                    else
                        TP[i] = new Point3D((Device.WiimoteState.IRState.IRSensors[i].Position.X) * 1024,
                                                        (1 - Device.WiimoteState.IRState.IRSensors[i].Position.Y) * 768, Device.WiimoteState.IRState.IRSensors[i].Size);
                    
                    if (IsXReversed)
                        TP[i].X=1024-TP[i].X;
                    if (IsYReversed)
                        TP[i].Y=768-TP[i].Y;
                    Results[i]=i+"| "+TP[i].X.ToString("#.#")+","+TP[i].Y.ToString("#.#");
                    }
                return Results;
                }
            }


        private void Device_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
            {
            SWatch.Stop();
            _Rate = 1000.0 / SWatch.ElapsedMilliseconds;
            SWatch.Restart();
            ParseWiiData();

            App.Current.Dispatcher.Invoke(
            DispatcherPriority.Normal,
                  (Action)(() =>
                  {
                      if (App.Current.MainWindow != null)
                          (App.Current.MainWindow as MainWindow).NewSamplesReady = true;
                  }));

            //SWatch.Start();
            }

        private void ParseWiiData()
            {
            OnPropertyChanged("_TargetPositions3D");
            OnPropertyChanged("_TargetLabels");
            OnPropertyChanged("_Battery");
            }

        public Point3D Position = new Point3D();

        public bool Connect()
            {
            try
                {
                Device.SetLEDs(true, true, true, true);
                }
            catch { }
            //Device.SetRumble(true);
            Device.WiimoteChanged += new EventHandler<WiimoteChangedEventArgs>(Device_WiimoteChanged);
            //Device.SetReportType(InputReport.Status,IRSensitivity.WiiLevel1,true);
            try
                {
                Device.Connect();
                }
            catch (Exception E)
                {
                Debug.WriteLine(E.Message);
                //return false;
                }

            IRSensitivity IRS = IRSensitivity.Maximum;
            App.Current.Dispatcher.Invoke(
            DispatcherPriority.Normal,
                  (Action)(() =>
                  {
                      IRS = (IRSensitivity)(App.Current.MainWindow as MainWindow)._IRSensitivity;
                  }));

            Device.SetReportType(InputReport.IRAccel, IRS, true);
            // SetLEDs(true);
            //remote.GetBatteryLevel();
            Device.SetLEDs(true, false, false, true);
            //Device.WiimoteChanged += new EventHandler<WiimoteChangedEventArgs>(Device_WiimoteChanged);
            SWatch.Start();
            //Device.SetRumble(false);
            return true;
            }

        private void SetLEDs(bool Active)
            {
            if (Device == null) return;
            Device.SetLEDs(false, false, false, false);
            Thread.SpinWait(500);
            if (Active)
                Device.SetLEDs(true, false, false, true);
            else
                Device.SetLEDs(true, true, false, false);
            }

        private void Close()
            {
            SWatch.Stop();
            Device.WiimoteChanged -= Device_WiimoteChanged;
            try
                {
                SetLEDs(false);
                Device.Disconnect();
                }
            catch { }
            //Device=null;
            }

        [NonSerialized, XmlIgnore]
        private Wiimote Device;

        [XmlIgnore]
        public Wiimote _Device
            {
            get { return Device; }
            set { Device = value; }
            }

        private double Rate;

        public double _Rate
            {
            get { return Rate; }
            set
                {
                Rate = value;
                OnPropertyChanged("_Rate");
                }
            }

        public double _Battery
            {
            get
                {
                if (Device == null) return 0;
                return Device.WiimoteState.Battery;
                }
            }

        public string _ID
            {
            get { return Device.ID.ToString(); }
            }

        public IntrinsicCameraParameters IP;
        public ExtrinsicCameraParameters[] ECP;

        public MatrixTransform3D GetIntrinsicPoseTransform(int ViewIndex)
            {
            if (ViewIndex > 0)
                return new MatrixTransform3D(Helper3D.RevertTransformationMatrix(GetTransformMatrix(ViewIndex)));
            else
                return new MatrixTransform3D(Helper3D.RevertTransformationMatrix(GetTransformMatrix(0)));
            }

        public Matrix3D GetTransformMatrix(int view)
            {
            Matrix<double> M = R;
           

            if ((ECP == null) || (view == -3))
                {
                Matrix3D M3D1 = new Matrix3D(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
                // Matrix3D M3D1=new Matrix3D(1,0,0,0, 0,-1,0,0, 0,0,-1,0, 0,0,0,1);
                return M3D1;
                }
             if (view == -5)
                {
                //M=R;
                //this.
                Matrix<double> T1 = ECP[0].TranslationVector;
                double AD=Math.Sqrt(Math.Pow(T1[0, 0],2)+(Math.Pow(T1[1, 0],2)+(Math.Pow(T1[2, 0],2))));
                double ca=Math.Cos(Math.PI);
                double si=Math.Sin(Math.PI);
                //Matrix3D M3D1 = new Matrix3D(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, AD, 1);
                Matrix3D M3D1 = new Matrix3D(ca, -si, 0, 0, si, ca, 0, 0, 0, 0, 1, 0, 0, 0, AD, 1);
                return M3D1;
                }
            if (view == -1)
                {
                //M=R;
                //this.
                Matrix3D M3D1 = new Matrix3D(M[0, 0], M[0, 1], M[0, 2], 0, M[1, 0], M[1, 1], M[1, 2], 0, M[2, 0], M[2, 1], M[2, 2], 0, 0, 0, 0, 1);
                return M3D1;
                }
            if (view == -2)
                {
                M = R;
                Matrix<double> T1 = ECP[0].TranslationVector;
                Matrix3D M3D1 = new Matrix3D(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, T1[0, 0], T1[1, 0], T1[2, 0], 1);
                return M3D1;
                }

            if (view >= ECP.Length)
                return Helper3D.BuildTransformationMatrix(ECP[0].RotationVector, ECP[0].TranslationVector);
            else
                return Helper3D.BuildTransformationMatrix(ECP[view].RotationVector, ECP[view].TranslationVector);

            //RotationVector3D V=new RotationVector3D();
            //V[0,0]=-ECP[view].RotationVector[0,0];
            //V[1,0]=-ECP[view].RotationVector[1,0];
            //V[2,0]=-ECP[view].RotationVector[2,0];
            //M=V.RotationMatrix;

            //Matrix<double> T=ECP[view].TranslationVector;//.RotationVector

            //Matrix3D M3D=new Matrix3D(M[0,0],M[0,1],M[0,2],0,M[1,0],M[1,1],M[1,2],0,M[2,0],M[2,1],M[2,2],0,T[0,0],T[1,0],T[2,0],1);
            ////M3D=(App.Current.MainWindow as MainWindow).RevertTransformationMatrix(M3D);

            // return M3D;
            }

        public Point3D V1;
        public Point3D V2;

        public Matrix<double> R = new Matrix<double>(3, 3); //rectification transforms (rotation matrices) for Camera.
        public Matrix<double> P = new Matrix<double>(3, 4); //projection matrices in the new (rectified) coordinate systems for Camera.

        public override string ToString()
            {
            return this._Name;
            }

        #region ICloneable Members

        public object Clone()
            {
            return this.MemberwiseClone();
            }

        #endregion ICloneable Members
        }

    public class CameraView
        {
        public CameraView()
            {
            }

        public CameraView(System.Drawing.PointF[] Targets, string CameraID)
            {
            TargetPositions = new List<System.Drawing.PointF>(Targets);
            this.CameraID = CameraID;
            RestoreCameraReference();
            Estimate3DMetrics();
            }

        public void RestoreCameraReference()
            {
            foreach (CameraInfo CI in (App.Current.MainWindow as MainWindow)._AvailableWiimotes)
                {
                if (CI._Index == CameraID)
                    {
                    Camera = CI;
                    break;
                    }
                }
            }

        public string CameraID;

        private CameraInfo Camera = null;

        [XmlIgnore]
        public CameraInfo _Camera
            {
            get
                {
                return Camera;
                }
            }

        private List<System.Drawing.PointF> TargetPositions = new List<System.Drawing.PointF>();

        public List<System.Drawing.PointF> _TargetPositions
            {
            get { return TargetPositions; }
            }

        public double GetTargetPolygonArea()
            {
            // poligon
            double Area = 0;
            for (int l = 0; l < 4; l++)
                {
                int in1 = l;
                int in2 = l + 1;
                if (l == 3) in2 = 0;
                Area = Area + _TargetPositions[in1].X *
                                _TargetPositions[in2].Y -
                                _TargetPositions[in1].Y *
                                _TargetPositions[in2].X;
                }
            return Area / 2;
            }

        /// <summary>
        /// W,x,y,z
        /// </summary>
        double[] Quaternion = new double[4];

        public double[] _Quaternion
            {
            get { return Quaternion; }   
            set{  Quaternion =value;}        
            }


        public string _Angle
            {
            get{return ((Math.Abs(Quaternion[1])+Math.Abs(Quaternion[2]))/2).ToString("#.00");}
            }

        //bool MetricsAreEstimated = false;

        //public bool _MetricsAreEstimated
        //    {
        //    get { return MetricsAreEstimated; }
        //    set { MetricsAreEstimated = value; }
        //    }

        
        public void Estimate3DMetrics()
            {
            Quaternion = new double[4];
            if (TargetPositions.Count!=4) return;
            MatrixTransform3D CT;

            MainWindow MW = (App.Current.MainWindow as MainWindow);

            Emgu.CV.Structure.MCvPoint3D32f[] P1 = new Emgu.CV.Structure.MCvPoint3D32f[4];
            Emgu.CV.Structure.MCvPoint3D32f[] P2 = new Emgu.CV.Structure.MCvPoint3D32f[4];
            
            System.Drawing.PointF[] PF1=new System.Drawing.PointF[4];
            System.Drawing.PointF[] PF2=new System.Drawing.PointF[4];



            Point3D[] M;
            M = new Point3D[4];

            for (int p = 0; p < 4; p++)
                {
                //Point3D P = new Point3D(MW.CalibrationToken[p].x, MW.CalibrationToken[p].y, MW.CalibrationToken[p].z);
                //Point3D TP = CT.Transform(P);
               // M[p] = TP;
                P1[p] = new Emgu.CV.Structure.MCvPoint3D32f(MW.CalibrationToken[p].x, MW.CalibrationToken[p].y, MW.CalibrationToken[p].z);
                P2[p] = new Emgu.CV.Structure.MCvPoint3D32f((float)TargetPositions[p].X, (float)TargetPositions[p].Y, (float)0);
               
                PF1[p]= new System.Drawing.PointF(MW.CalibrationToken[p].x, MW.CalibrationToken[p].y);
                PF2[p]= new System.Drawing.PointF((float)TargetPositions[p].X, (float)TargetPositions[p].Y);
                //Helper3D.Add3DMarker(vCameras.Children,TP,0.1,BrushSet.GetBrush(p+1).Color);
                }

            Matrix<double> MD = null;
            Matrix<double> RD = null;
            byte[] BT = null;
           // Emgu.CV.CvInvoke.cvCreatePOSITObject(.cvPOSIT(
            Emgu.CV.CvInvoke.CvEstimateAffine3D(P1, P2, out MD, out BT, 3, 0.6);
            Matrix3D MR;
            double m1,m2;
          
             using (Matrix<float> ll = new Matrix<float>(2,3))
                {
                Matrix<float> l3 = new Matrix<float>(3,3);
                Emgu.CV.CvInvoke.cvGetAffineTransform(PF1,PF2,ll);

                Emgu.CV.CvInvoke.cvGetPerspectiveTransform(PF1,PF2,l3);
                MR = Helper3D.BuildTransformationMatrix(MD);
                MR.SetIdentity();
                /*MR.M11=ll[0,0];
                MR.M12=ll[0,1];
                MR.M14=ll[0,2];


                MR.M21=ll[1,0];
                MR.M22=ll[1,1];
                MR.M24=ll[1,2];*/



                MR.M11=l3[0,0];
                MR.M12=l3[0,1];
                MR.M13=l3[0,2];

                MR.M21=l3[1,0];
                MR.M22=l3[1,1];
                MR.M23=l3[1,2];

                MR.M31=l3[2,0];
                MR.M32=l3[2,1];
                MR.M33=l3[2,2];
               
               Vector3D gX=Vector3D.Multiply(new Vector3D(-0.5,0.5,0),MR);

             
                //m1=180.0*Math.PI*Math.Atan2(MR.M22,MR.M21)-90;
                //m1=m1%360;
                //m2=180.0*Math.PI*Math.Atan2(MR.M12,MR.M11);
                //m2=m2%360;
                }
                
            

           
            /// algorrithm by Copyright (c) 1998-2014 Martin John Baker - All rights reserved
            /// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
          
            double tr = MR.M11 + MR.M22 + MR.M33;

            if (tr > 0) { 
              double S = Math.Sqrt(tr+1.0) * 2; // S=4*qw 
              Quaternion[0] = 0.25 * S;
              Quaternion[1] = (MR.M32 - MR.M23) / S;
              Quaternion[2] = (MR.M13 - MR.M31) / S; 
              Quaternion[3] = (MR.M21 - MR.M12) / S; 
            } else if ((MR.M11 > MR.M22)&(MR.M11 > MR.M33)) { 
              double S = Math.Sqrt(1.0 + MR.M11 - MR.M22 - MR.M33) * 2; // S=4*qx 
              Quaternion[0] = (MR.M32 - MR.M23) / S;
              Quaternion[1] = 0.25 * S;
              Quaternion[2] = (MR.M12 + MR.M21) / S; 
              Quaternion[3] = (MR.M13 + MR.M31) / S; 
            } else if (MR.M22 > MR.M33) { 
              double S = Math.Sqrt(1.0 + MR.M22 - MR.M11 - MR.M33) * 2; // S=4*qy
              Quaternion[0] = (MR.M13 - MR.M31) / S;
              Quaternion[1] = (MR.M12 + MR.M21) / S; 
              Quaternion[2] = 0.25 * S;
              Quaternion[3] = (MR.M23 + MR.M32) / S; 
            } else { 
              double S = Math.Sqrt(1.0 + MR.M33 - MR.M11 - MR.M22) * 2; // S=4*qz
              Quaternion[0] = (MR.M21 - MR.M12) / S;
              Quaternion[1] = (MR.M13 + MR.M31) / S;
              Quaternion[2] = (MR.M23 + MR.M32) / S;
              Quaternion[3] = 0.25 * S;
            }
          
            Quaternion g=new Quaternion(Quaternion[1],Quaternion[2],Quaternion[3],Quaternion[0]);
            


            System.Windows.Media.Media3D.Matrix3D m = System.Windows.Media.Media3D.Matrix3D.Identity;
					m.Rotate(g);
                    
					Vector3D AX=Vector3D.Multiply(new Vector3D(-0.5,0.5,0),m);
					Vector3D AY=Vector3D.Multiply(new Vector3D(1,1,0),m);
					Vector3D AZ=Vector3D.Multiply(new Vector3D(0,0,1),m);




            double T=g.Angle;
            double SA=Math.Abs(g.Axis.X*g.Angle)+Math.Abs(g.Axis.Y*g.Angle);


            Quaternion[0]=g.Axis.X*g.Angle;

            Quaternion[1]=g.Axis.Y*g.Angle;
            Quaternion[2]=g.Axis.Z*g.Angle;
            Quaternion[3]=0;

            //Quaternion[0]=0;

            //Quaternion[1]=m1;
            //Quaternion[2]=m2;
            //Quaternion[3]=0;

            }

        public override string ToString()
            {
            int ViewIndex = -1;
            Application.Current.Dispatcher.Invoke(new Action(() =>
               ViewIndex = (App.Current.MainWindow as MainWindow)._SelectedCameraViews.IndexOf(this) + 1));

            return "View " + ViewIndex;
            }
        }

    public class ViewsetStereoError
        {
        public ViewsetStereoError()
            {
            }

        public ViewsetStereoError(string Camera1, string Camera2, double Error)
            {
            this.Camera1 = Camera1;
            this.Camera2 = Camera2;
            this.Error = Error;
            }

        private string Camera1;

        public string _Camera1
            {
            get { return Camera1; }
            set { Camera1 = value; }
            }

        private string Camera2;

        public string _Camera2
            {
            get { return Camera2; }
            set { Camera2 = value; }
            }

        private double Error;

        public double _Error
            {
            get { return Error; }
            set { Error = value; }
            }
        }

    public class CalibrationViewset
        {
        public CalibrationViewset()
            {
            }

        public CalibrationViewset(List<CameraView> Views)
            {
            SnapshotViews = new AsyncObservableCollection<CameraView>(Views);
            }

        bool IsSelected = false;

        public bool _IsSelected
            {
            get { return IsSelected; }
            set { IsSelected = value; }
            }


        public CalibrationViewset(IList<CameraInfo> ActiveWiimotes)
            {
            //List<float> TargetArrangmentSign=new List<float>();

            for (int n = 0; n < ActiveWiimotes.Count; n++)
                {
                CameraInfo Camera = ActiveWiimotes[n];
                System.Drawing.PointF[] P = Camera.GetCurrentTarget2DPositions();
                bool EarlierEdgeDirection = false;

                bool IsValidTargetArrangment = true;

                for (int i = 0; i < P.Length; i++)
                    {
                    if (P[i].X < 1 || P[i].X > 1023 || P[i].Y < 1 || P[i].Y > 767) /// rule out the null (ubdetected) positions, denoted by Wiimote as 0.50003, 1023.666
                        {
                        IsValidTargetArrangment = false;
                        break;
                        }
                    }
                if (P.Length < 4) IsValidTargetArrangment = false;
                if (IsValidTargetArrangment)
                    {
                    System.Drawing.PointF Center = new System.Drawing.PointF();
                    for (int i = 0; i < P.Length; i++)
                        {
                        Center.X = Center.X + P[i].X;
                        Center.Y = Center.Y + P[i].Y;
                        }
                    Center.X = Center.X / 4.0f;
                    Center.Y = Center.Y / 4.0f;
                    System.Collections.Generic.List<double> Theta = new System.Collections.Generic.List<double>();
                    for (int i = 0; i < P.Length; i++)
                        {
                        double Angle = Math.Atan2((double)Center.Y - P[i].Y, (double)P[i].X - Center.X) * 180.0 / Math.PI;
                        if (Angle < 0) Angle = Angle + 360;
                        Theta.Add(Angle);
                        }
                    double D = Theta[0];
                    for (int i = 0; i < P.Length; i++)
                        {
                        Theta[i] = Theta[i] - D;
                        if (Theta[i] < 0) Theta[i] = Theta[i] + 360;
                        }

                    bool Clockwise = true;
                    bool AntiClockwise = true;

                    for (int i = 1; i < P.Length; i++)
                        {
                        int s = i + 1;
                        if (s > P.Length - 1) s = 0;
                        if (Theta[i - 1] >= Theta[i])
                            AntiClockwise = false;
                        if (Theta[i] <= Theta[s])
                            Clockwise = false;
                        }

                    IsValidTargetArrangment = Clockwise;

                    ///IsValidTargetArrangment= Clockwise||AntiClockwise;

                    //);

                    //for (int i = 0; i < P.Length - 1; i++)
                    //    {
                    //    System.Drawing.PointF[] Triangle = new System.Drawing.PointF[4];
                    //    for (int l = 0; l < 3; l++)
                    //        {
                    //        if ((i + l) < P.Length)
                    //            Triangle[l] = P[i + l];
                    //        else
                    //            Triangle[l] = P[0];
                    //        }

                    //    Triangle[3] = P[i];
                    //    bool EdgeDirection = IsClockwisePolygon(Triangle);
                    //    if ((i > 0) && (EdgeDirection != EarlierEdgeDirection))
                    //        {
                    //        IsValidTargetArrangment = false;
                    //        break;
                    //        }
                    //    EarlierEdgeDirection = EdgeDirection;
                    //    }
                    }

                if (IsValidTargetArrangment)
                    {
                    SnapshotViews.Add(new CameraView(P, Camera._Index));
                    }
                }
            IsValidShot = SnapshotViews.Count > 0;
            }

        /// <summary>
        /// Copyright Domininoc925
        /// http://dominoc925.blogspot.nl/2012/03/c-code-to-determine-if-polygon-vertices.html
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        private bool IsClockwisePolygon(System.Drawing.PointF[] polygon)
            {
            bool isClockwise = false;
            double sum = 0;
            for (int i = 0; i < polygon.Length - 1; i++)
                {
                sum += (polygon[i + 1].X - polygon[i].X) * (polygon[i + 1].Y + polygon[i].Y);
                }
            isClockwise = (sum < 0) ? true : false;
            return isClockwise;
            }

        public bool IsValidShot = true;

        private AsyncObservableCollection<CameraView> SnapshotViews = new AsyncObservableCollection<CameraView>();

        public AsyncObservableCollection<CameraView> _CameraViews
            {
            get { return SnapshotViews; }
            }

        //private AsyncObservableCollection<System.Drawing.PointF[]> Target2DPositions = new AsyncObservableCollection<System.Drawing.PointF[]>();

        //public AsyncObservableCollection<System.Drawing.PointF[]> _Target2DPositions {
        //    get { return Target2DPositions; }
        //}

        private AsyncObservableCollection<ViewsetStereoError> ViewsetStereoErrors = new AsyncObservableCollection<ViewsetStereoError>();

        public AsyncObservableCollection<ViewsetStereoError> _ViewsetStereoErrors
            {
            get { return ViewsetStereoErrors; }
            }

        public void AddError(CameraInfo W1, CameraInfo W2, double Error)
            {
            ViewsetStereoError NewItem = new ViewsetStereoError(W1._Name, W2._Name, Error);
            ViewsetStereoError ReplaceItem = null;
            foreach (ViewsetStereoError Key in ViewsetStereoErrors)
                {
                if (((Key._Camera1 == W1._Name) && (Key._Camera2 == W2._Name)) ||
                    ((Key._Camera2 == W1._Name) && (Key._Camera1 == W2._Name)))
                    {
                    ReplaceItem = Key;
                    break;
                    }
                }

            if (ReplaceItem != null)
                ViewsetStereoErrors.Remove(ReplaceItem);

            ViewsetStereoErrors.Add(NewItem);
            }

        private System.DateTime TimeStamp = DateTime.Now;

        public override string ToString()
            {
            string S = this.TimeStamp.ToShortTimeString() + " ** ";
            foreach (CameraView SV in SnapshotViews)
                {
                S = S + SV._Camera._Name + " | ";
                }
            return S;
            }

        public bool ContainsWii(CameraInfo WI)
            {
            foreach (CameraView SV in SnapshotViews)
                {
                if (SV._Camera == WI) return true;
                }
            return false;
            }

        public System.Drawing.PointF[] GetTargetPositions(CameraInfo WI)
            {
            foreach (CameraView SV in SnapshotViews)
                {
                if (SV._Camera == WI) return SV._TargetPositions.ToArray();
                }
            return null;
            }

        public void ResetIntrinsicCalibration()
            {
            foreach (CameraView SV in SnapshotViews)
                {
                SV._Camera._IsIntrinsicCalibrated = false;
                }
            }
        }

    public class SetupDescription
        {
        public SetupDescription()
            {
            }

        public List<CalibrationViewset> CSnapshots = new List<CalibrationViewset>();
        public List<StereoCorrespondence> StereoCorrespondences = new List<StereoCorrespondence>();
        public List<CameraInfo> AvailableWiimotes = new List<CameraInfo>();
        public Matrix3D WorldTranslationInLog;
        }

    public class LogEntry : INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
            {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                        {
                            PropertyChangedEventHandler handler = PropertyChanged;
                            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
                        }));
            }

        public DateTime DateTime { get; set; }

        public int Index { get; set; }

        public string Message { get; set; }
        }

    public class TrackingResult : INotifyPropertyChanged
        {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }

        #endregion INotifyPropertyChanged Members

        private Point3D Position3D;

        public string _Position3D
            {
            get
                {
                if (double.IsNaN(Position3D.X)) return "";
                return "(" + Position3D.X.ToString("0.00") + "," + Position3D.Y.ToString("0.00") + "," + Position3D.Z.ToString("0.00") + ")";
                }
            }

        public Point3D _TargetPosition
            {
            get { return Position3D; }
            set
                {
                Position3D = value;
                OnPropertyChanged("_Position3D");
                OnPropertyChanged("_ConfidenceScore");
                }
            }

        public int CaptorNumber=0;
        public double UncertantyScore=0;

            

        public string _ConfidenceScore
            {
            get {
                return "N="+CaptorNumber+" | Er="+UncertantyScore.ToString("#.00");;
                }
            }
        }
    }