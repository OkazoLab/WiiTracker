﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods that carry camera calibration and estimate stereo correspondences
// </copyright>
      

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Media;
using Emgu.CV;
using Emgu.CV.Structure;
using Fluent;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow
        {
        #region Calibration Token

        /// <summary>
        ///  best so far
        public MCvPoint3D32f[] CalibrationToken = new MCvPoint3D32f[4] { new MCvPoint3D32f(0, 1, 0), new MCvPoint3D32f(1, 1, 0), new MCvPoint3D32f(1, 0, 0), new MCvPoint3D32f(0, 0, 0) };

        // MCvPoint3D32f[] CalibrationToken=new MCvPoint3D32f[4]{new MCvPoint3D32f(0,0,0),new MCvPoint3D32f(1,0,0),new MCvPoint3D32f(1,1,0),new MCvPoint3D32f(0,1,0)};

        // MCvPoint3D32f[] CalibrationToken=new MCvPoint3D32f[4]{new MCvPoint3D32f(1,1,0),new MCvPoint3D32f(0,1,0),new MCvPoint3D32f(0,0,0),new MCvPoint3D32f(1,0,0)};

        public string _CalibrationTokenPosition1
            {
            get { return CalibrationToken[0].x.ToString("0.##") + " , " + CalibrationToken[0].y.ToString("0.##") + " , " + CalibrationToken[0].z.ToString("0.##"); }
            set
                {
                string[] numbers = Regex.Split(value, @"\D+");
                try
                    {
                    if (numbers.Length == 3)
                        {
                        CalibrationToken[0].x = Single.Parse(numbers[0]);
                        CalibrationToken[0].y = Single.Parse(numbers[1]);
                        CalibrationToken[0].z = Single.Parse(numbers[2]);
                        }
                    }
                catch { }
                OnPropertyChanged("_CalibrationTokenPosition1");
                }
            }

        public string _CalibrationTokenPosition2
            {
            get { return CalibrationToken[1].x.ToString("0.##") + " , " + CalibrationToken[1].y.ToString("0.##") + " , " + CalibrationToken[1].z.ToString("0.##"); }
            set
                {
                string[] numbers = Regex.Split(value, @"\D+");
                try
                    {
                    if (numbers.Length == 3)
                        {
                        CalibrationToken[1].x = Single.Parse(numbers[0]);
                        CalibrationToken[1].y = Single.Parse(numbers[1]);
                        CalibrationToken[1].z = Single.Parse(numbers[2]);
                        }
                    }
                catch { }
                OnPropertyChanged("_CalibrationTokenPosition2");
                }
            }

        public string _CalibrationTokenPosition3
            {
            get { return CalibrationToken[2].x.ToString("0.##") + " , " + CalibrationToken[2].y.ToString("0.##") + " , " + CalibrationToken[2].z.ToString("0.##"); }
            set
                {
                string[] numbers = Regex.Split(value, @"\D+");
                try
                    {
                    if (numbers.Length == 3)
                        {
                        CalibrationToken[2].x = Single.Parse(numbers[0]);
                        CalibrationToken[2].y = Single.Parse(numbers[1]);
                        CalibrationToken[2].z = Single.Parse(numbers[2]);
                        }
                    }
                catch { }
                OnPropertyChanged("_CalibrationTokenPosition3");
                }
            }

        public string _CalibrationTokenPosition4
            {
            get { return CalibrationToken[3].x.ToString("0.##") + " , " + CalibrationToken[3].y.ToString("0.##") + " , " + CalibrationToken[3].z.ToString("0.##"); }
            set
                {
                string[] numbers = Regex.Split(value, @"\D+");
                try
                    {
                    if (numbers.Length == 3)
                        {
                        CalibrationToken[3].x = Single.Parse(numbers[0]);
                        CalibrationToken[3].y = Single.Parse(numbers[1]);
                        CalibrationToken[3].z = Single.Parse(numbers[2]);
                        }
                    }
                catch { }
                OnPropertyChanged("_CalibrationTokenPosition4");
                }
            }

        #endregion Calibration Token

        private void RecalibrateSingleCamera(CameraInfo WI)
            {
            int n = 0;
            foreach (CalibrationViewset CS in CalibrationSnapshots)
                {
                if (CS.ContainsWii(WI)) n++;
                }

            if (n == 0) return;

            System.Drawing.Size imageSize = new System.Drawing.Size(1024, 768);
            MCvPoint3D32f[][] objectPoints = new MCvPoint3D32f[n][];
            System.Drawing.PointF[][] imagePoints = new System.Drawing.PointF[n][];
            for (int i = 0; i < n; i++)
                {
                objectPoints[i] = CalibrationToken;
                imagePoints[i] = new System.Drawing.PointF[4];
                }

            n = 0;
            foreach (CalibrationViewset CS in CalibrationSnapshots)
                {
                if (CS.ContainsWii(WI))
                    {
                    imagePoints[n] = CS.GetTargetPositions(WI);
                    n++;
                    }
                }

            WI.IP = new IntrinsicCameraParameters();
            Emgu.CV.CvEnum.CALIB_TYPE CT = Emgu.CV.CvEnum.CALIB_TYPE.DEFAULT;
            Emgu.CV.Structure.MCvTermCriteria TC = new MCvTermCriteria(10000);
            WI._MSE = CameraCalibration.CalibrateCamera(objectPoints, imagePoints, imageSize, WI.IP, CT, TC, out WI.ECP);
            
            WI._IsIntrinsicCalibrated = true;
            //WriteLogLine(WI._Name + " is re-calibrated");
            }

        private void EstimateStereoCorrespondence(CameraInfo WI1, CameraInfo WI2)
            {

            List<CalibrationViewset> SelectedSnapshots = new List<CalibrationViewset>();
            foreach (CalibrationViewset CS in CalibrationSnapshots)
                {
                if (CS.ContainsWii(WI1) && CS.ContainsWii(WI2))
                    SelectedSnapshots.Add(CS);
                }

            if (SelectedSnapshots.Count == 0) return;

            System.Drawing.Size imageSize = new System.Drawing.Size(1024, 768);

            MCvPoint3D32f[][] objectPoints = new MCvPoint3D32f[SelectedSnapshots.Count][];

            System.Drawing.PointF[][] imagePoints1 = new System.Drawing.PointF[SelectedSnapshots.Count][];
            System.Drawing.PointF[][] imagePoints2 = new System.Drawing.PointF[SelectedSnapshots.Count][];

            for (int i = 0; i < SelectedSnapshots.Count; i++)
                {
                objectPoints[i] = CalibrationToken;
                // imagePoints1[i]=new System.Drawing.PointF[4];
                // imagePoints2[i]=new System.Drawing.PointF[4];
                }

            for (int i = 0; i < SelectedSnapshots.Count; i++)
                {
                CalibrationViewset CS = SelectedSnapshots[i];
                imagePoints1[i] = CS.GetTargetPositions(WI1);

                imagePoints2[i] = CS.GetTargetPositions(WI2);
                }
            ExtrinsicCameraParameters EP = null;
            Matrix<double> FMD;
            Matrix<double> EMD;

            double Error = 0;

            try
                {
                if (!WI1._IsIntrinsicCalibrated)
                    RecalibrateSingleCamera(WI1);
                if (!WI2._IsIntrinsicCalibrated)
                    RecalibrateSingleCamera(WI2);

                CameraCalibration.StereoCalibrate(objectPoints, imagePoints2, imagePoints1, WI2.IP, WI1.IP, imageSize
                      ,Emgu.CV.CvEnum.CALIB_TYPE.CV_CALIB_FIX_PRINCIPAL_POINT,//);//..CV_CALIB_USE_INTRINSIC_GUESS,
                         new MCvTermCriteria(10000), out EP, out FMD, out EMD);

                Error = ComputeEpipolarCalibrationError(imagePoints2, WI2.R, WI2.P, WI2.IP.IntrinsicMatrix, WI2.IP.DistortionCoeffs, imagePoints1, WI1.R, WI1.P, WI1.IP.IntrinsicMatrix, WI1.IP.DistortionCoeffs, FMD, SelectedSnapshots, WI1, WI2);
                }
            catch
                {
                WriteErrorLogLine("Unknown error in estimating a stereo correspondence between " + WI1._Name + " and " + WI2._Name);
                return;
                }
            //Matrix<float> Lines2=ComputeEpipolarCalibrationError(imagePoints2,WI2.R1,WI2.P1,WI2.IP.IntrinsicMatrix,WI2.IP.DistortionCoeffs,2,FMD);

            Matrix<double> Q = new Matrix<double>(4, 4); //This is what were interested in the disparity-to-depth mapping matrix
           
            StereoCorrespondence CP = new StereoCorrespondence(WI1,WI2,WI1._Index, WI2._Index, EP, Q, SelectedSnapshots.Count, Error);
            MeasuredStereoCorrespondences.Add(CP);
          
            }

     
        }
    }