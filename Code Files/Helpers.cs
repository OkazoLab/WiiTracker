﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods for 3D calculations and also several type extensions
// </copyright>
      
      
using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Emgu.CV;

namespace WiiTracker
    {
    public static class Helper3D
        {
        public static Point3D FindBestIntersection(Point3D P1, Vector3D V1, Point3D P2, Vector3D V2, out Point3D R1, out Point3D R2)
            {
            double EPS = 0.00000001;

            Vector3D V3 = new Vector3D();
            V3.X = P1.X - P2.X;
            V3.Y = P1.Y - P2.Y;
            V3.Z = P1.Z - P2.Z;

            double a = V1.X * V1.X + V1.Y * V1.Y + V1.Z * V1.Z;
            double b = V1.X * V2.X + V1.Y * V2.Y + V1.Z * V2.Z;//dot(V1, V2);
            double c = V2.X * V2.X + V2.Y * V2.Y + V2.Z * V2.Z;//dot(V2, V2);
            double d = V1.X * V3.X + V1.Y * V3.Y + V1.Z * V3.Z;//dot(V1, V3);
            double eee = V2.X * V3.X + V2.Y * V3.Y + V2.Z * V3.Z;//dot(V2, V3);
            double DDD = a * c - b * b;

            double sc, sN, sD = DDD;
            double tc, tN, tD = DDD;

            if (DDD < EPS)
                {
                sN = 0.0;
                sD = 1.0;
                tN = eee;
                tD = c;
                }
            else
                {
                sN = (b * eee - c * d);
                tN = (a * eee - b * d);
                if (sN < 0.0)
                    {
                    sN = 0.0;
                    tN = eee;
                    tD = c;
                    }
                else if (sN > sD)
                    {
                    sN = sD;
                    tN = eee + b;
                    tD = c;
                    }
                }

            if (tN < 0.0)
                {
                tN = 0.0;

                if (-d < 0.0)
                    sN = 0.0;
                else if (-d > a)
                    sN = sD;
                else
                    {
                    sN = -d;
                    sD = a;
                    }
                }
            else if (tN > tD)
                {
                tN = tD;
                if ((-d + b) < 0.0)
                    sN = 0;
                else if ((-d + b) > a)
                    sN = sD;
                else
                    {
                    sN = (-d + b);
                    sD = a;
                    }
                }

            if (Math.Abs(sN) < EPS) sc = 0.0;
            else sc = sN / sD;
            if (Math.Abs(tN) < EPS) tc = 0.0;
            else tc = tN / tD;

            R1 = new Point3D();
            R1.X = P1.X + sc * V1.X;
            R1.Y = P1.Y + sc * V1.Y;
            R1.Z = P1.Z + sc * V1.Z;

            R2 = new Point3D();
            R2.X = P2.X + tc * V2.X;
            R2.Y = P2.Y + tc * V2.Y;
            R2.Z = P2.Z + tc * V2.Z;

            return new Point3D((R1.X + R2.X) / 2.0, (R1.Y + R2.Y) / 2.0, (R1.Z + R2.Z) / 2.0);
            }

        public static Matrix3D BuildTransformationMatrix(Matrix<double> MI)
            {
            Matrix3D M = new Matrix3D();
            M.M11 = MI[0, 0];
            M.M12 = MI[0, 1];
            M.M13 = MI[0, 2];
            M.M14 = 0;

            M.M21 = MI[1, 0];
            M.M22 = MI[1, 1];
            M.M23 = MI[1, 2];
            M.M24 = 0;

            M.M31 = MI[2, 0];
            M.M32 = MI[2, 1];
            M.M33 = MI[2, 2];
            M.M34 = 0;

            M.M44 = 1;

            M.OffsetX = MI[0, 3];
            M.OffsetY = MI[1, 3];
            M.OffsetZ = MI[2, 3];

            return M;
            }

        public static Matrix3D RevertTransformationMatrix(Matrix3D MM)
            {
            Matrix3D M = new Matrix3D();
            M.M11 = MM.M11;
            M.M12 = MM.M21;
            M.M13 = MM.M31;

            M.M21 = MM.M12;
            M.M22 = MM.M22;
            M.M23 = MM.M32;

            M.M31 = MM.M13;
            M.M32 = MM.M23;
            M.M33 = MM.M33;

            M.OffsetX = -MM.M11 * MM.OffsetX - MM.M12 * MM.OffsetY - MM.M13 * MM.OffsetZ;
            M.OffsetY = -MM.M21 * MM.OffsetX - MM.M22 * MM.OffsetY - MM.M23 * MM.OffsetZ;
            M.OffsetZ = -MM.M31 * MM.OffsetX - MM.M32 * MM.OffsetY - MM.M33 * MM.OffsetZ;

            return M;
            }

        public static void Add3DMarker(Visual3DCollection ParentCollection, Point3D Center, double Radius, SolidColorBrush MarkerBrush,bool IsCrossMarker)
            {

            if (IsCrossMarker)
                {
                Color MarkerColor=MarkerBrush.Color;
                HelixToolkit.Wpf.LinesVisual3D XLine = new HelixToolkit.Wpf.LinesVisual3D();
                XLine.Points.Add(new Point3D(Center.X - Radius, Center.Y, Center.Z));
                XLine.Points.Add(new Point3D(Center.X + Radius, Center.Y, Center.Z));
                XLine.Thickness = Radius * 50;
                XLine.Color = MarkerColor;

                HelixToolkit.Wpf.LinesVisual3D YLine = new HelixToolkit.Wpf.LinesVisual3D();
                YLine.Points.Add(new Point3D(Center.X, Center.Y - Radius, Center.Z));
                YLine.Points.Add(new Point3D(Center.X, Center.Y + Radius, Center.Z));
                YLine.Thickness = Radius * 50;
                YLine.Color = MarkerColor;

                HelixToolkit.Wpf.LinesVisual3D ZLine = new HelixToolkit.Wpf.LinesVisual3D();
                ZLine.Points.Add(new Point3D(Center.X, Center.Y, Center.Z - Radius));
                ZLine.Points.Add(new Point3D(Center.X, Center.Y, Center.Z + Radius));
                ZLine.Thickness = Radius * 50;
                ZLine.Color = MarkerColor;

                ParentCollection.Add(XLine);
                ParentCollection.Add(YLine);
                ParentCollection.Add(ZLine);
                }
                else
                {
                HelixToolkit.Wpf.SphereVisual3D Sphere=new HelixToolkit.Wpf.SphereVisual3D();
                Sphere.Radius=Radius;
                Sphere.Fill=MarkerBrush;
                Sphere.Center=Center;
                ParentCollection.Add(Sphere);
                }
            }

        public static Matrix3D BuildTransformationMatrix(RotationVector3D RotationVector, Matrix<double> TranslationVector)
            {
            RotationVector3D V = new RotationVector3D();
            V[0, 0] = -RotationVector[0, 0];
            V[1, 0] = -RotationVector[1, 0];
            V[2, 0] = -RotationVector[2, 0];
            Matrix<double> M = V.RotationMatrix;
            Matrix<double> T = TranslationVector;//.RotationVector

            Matrix3D M3D = new Matrix3D(M[0, 0], M[0, 1], M[0, 2], 0, M[1, 0], M[1, 1], M[1, 2], 0, M[2, 0], M[2, 1], M[2, 2], 0, T[0, 0], T[1, 0], T[2, 0], 1);
            //M3D=(App.Current.MainWindow as MainWindow).RevertTransformationMatrix(M3D);

            return M3D;
            }
        
        /// <summary>
        /// http://stackoverflow.com/questions/4917400/nearest-neighbours-using-quaternions
        /// </summary>

        public static double QuaternionDistance(double[] q1,double[] q2)
            {
            double theta=Math.Sqrt(Math.Pow(q1[1]-q2[1],2) + Math.Pow(q1[2]-q2[2],2) +Math.Pow(q1[3]-q2[3],2));
            //double theta = Math.Acos(q1[0]*q2[0] + q1[1]*q2[1] + q1[2]*q2[2] + q1[3]*q2[3]);
            //if (theta>(Math.PI/2.0)) theta = Math.PI - theta;
            return theta;
            }

        public static Vector3D GetEulerAngles(Matrix3D M)
            {
            Vector3D V=new Vector3D(0,0,0);
            try{
                V.X = Math.Atan(M.M32/M.M33)*180.0/Math.PI;
                V.Y= -Math.Asin(M.M31)*180.0/Math.PI;
                V.Z=Math.Atan(M.M21/M.M11)*180.0/Math.PI;
                }
                catch{}
            return V;
            }
        }

    public static class TypeExtensions
        {
        public static bool IsValid(this Point3D P)
            {
            return !(Double.IsNaN(P.X)||Double.IsNaN(P.Y)||Double.IsNaN(P.Z));
            }

         public static bool IsZero(this Point3D P)
            {
            return (P.X==0)&&(P.Y==0)&&(P.Z==0);
            }
        }
    }