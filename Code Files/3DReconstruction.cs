﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods handling reconstruction of 3D camera poses
// </copyright>
      

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using Emgu.CV;
using Fluent;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow
        {
        private void bEstimateIntristicCamerasPositions_Click(object sender, RoutedEventArgs e)
            {
            
            ShowCameras(true);
            }

        private void bEstimateGlobalCamerasPositions_Click(object sender, RoutedEventArgs e)
            {
            ShowCameras(false);
            }

        private void ShowCameras(bool Intristic)
            {
            vCameras.Children.Clear();

            if (!Intristic)
                ReconstructWiimotePositions();

            List<Point3D> CameraFrame = new List<Point3D>();
            for (int i = 0; i < 18; i++)
                CameraFrame.Add(new Point3D());

            for (int i = 0; i < ActiveWiimotes.Count; i++)
                {
                double SensorWidth = 1.5;
                double SensorHeight = 1.125;
                if (_ActiveWiimotes[i].IP == null)
                    {
                    WriteErrorLogLine("Wiimote " + _ActiveWiimotes[i]._Name + " is not intrinsically calibrated!");
                    continue;
                    }

                double CX = _ActiveWiimotes[i].IP.IntrinsicMatrix[0, 2] / 1024.0 * 1.5; // in mm
                double CY = _ActiveWiimotes[i].IP.IntrinsicMatrix[1, 2] / 768.0 * 1.125; // in mm
                double f = (_ActiveWiimotes[i].IP.IntrinsicMatrix[0, 0] / 1024.0 * 1.5 + _ActiveWiimotes[i].IP.IntrinsicMatrix[1, 1] / 768.0 * 1.125) / 2.0; // in mm

                CameraFrame[0] = new Point3D(-CX, -CY, 0);
                CameraFrame[1] = new Point3D(SensorWidth - CX, -CY, 0);

                CameraFrame[2] = new Point3D(SensorWidth - CX, -CY, 0);
                CameraFrame[3] = new Point3D(SensorWidth - CX, SensorHeight - CY, 0);

                CameraFrame[4] = new Point3D(SensorWidth - CX, SensorHeight - CY, 0);
                CameraFrame[5] = new Point3D(-CX, SensorHeight - CY, 0);

                CameraFrame[6] = new Point3D(-CX, SensorHeight - CY, 0);
                CameraFrame[7] = new Point3D(-CX, -CY, 0);

                CameraFrame[8] = new Point3D(-CX, -CY, 0);
                CameraFrame[9] = new Point3D(0, 0, -f);

                CameraFrame[10] = new Point3D(SensorWidth - CX, -CY, 0);
                CameraFrame[11] = new Point3D(0, 0, -f);

                CameraFrame[12] = new Point3D(SensorWidth - CX, SensorHeight - CY, 0);
                CameraFrame[13] = new Point3D(0, 0, -f);

                CameraFrame[14] = new Point3D(-CX, SensorHeight - CY, 0);
                CameraFrame[15] = new Point3D(0, 0, -f);

                CameraFrame[16] = new Point3D(0, -CY, 0);
                CameraFrame[17] = new Point3D(0, -CY - 0.2, 0);

                List<Point3D> CPOriginal = new List<Point3D>(CameraFrame);

                HelixToolkit.Wpf.LinesVisual3D LV = new HelixToolkit.Wpf.LinesVisual3D();
                MatrixTransform3D OO = null;
                List<Point3D> CP = new List<Point3D>(CameraFrame);

                if (Intristic)
                    {
                    //MatrixTransform3D RT=new MatrixTransform3D(RevertTransformationMatrix(_ActiveWiimotes[i].GetStereoTransformMatrix(0)));
                    OO = _ActiveWiimotes[i].GetIntrinsicPoseTransform(cbBaseCameraView.SelectedIndex);
                    for (int l = 0; l < CP.Count; l++)
                        {
                        CP[l] = OO.Transform(CP[l]);
                        }
                    }
                else
                    {
                    OO = _ActiveWiimotes[i]._GlobalPoseTransform;
                    if (OO == null) continue;
                    for (int l = 0; l < CP.Count; l++)
                        {
                        CP[l] = OO.Transform(CP[l]);
                        }
                    }

                foreach (Point3D P in CP)
                    {
                    LV.Points.Add(P);
                    }

                LV.Thickness = 5.0;
                LV.Color = (_ActiveWiimotes[i]._CameraBrush as SolidColorBrush).Color;
                vCameras.Children.Add(LV);

                //HelixToolkit.Wpf.LinesVisual3D LVOriginal=new HelixToolkit.Wpf.LinesVisual3D();
                // LVOriginal.Thickness=5.0;
                // LVOriginal.Color=(_ActiveWiimotes[i]._CameraBrush as SolidColorBrush).Color;
                //  foreach (Point3D P in CPOriginal)
                //    {
                //    LVOriginal.Points.Add(P);
                //    }

                //vCameras.Children.Add(LVOriginal);

                if (Intristic)
                    {
          

                    MatrixTransform3D CT;
                    
                    
                    if (cbBaseCameraView.SelectedIndex >= 0)
                        CT = new MatrixTransform3D(_ActiveWiimotes[i].GetTransformMatrix(cbBaseCameraView.SelectedIndex));
                        else
                        CT = new MatrixTransform3D(_ActiveWiimotes[i].GetTransformMatrix(0));

                    Emgu.CV.Structure.MCvPoint3D32f[] P1 = new Emgu.CV.Structure.MCvPoint3D32f[4];
                    Emgu.CV.Structure.MCvPoint3D32f[] P2 = new Emgu.CV.Structure.MCvPoint3D32f[4];
                    Point3D[] M;
                    M = new Point3D[4];

                    for (int p = 0; p < 4; p++)
                        {
                        Point3D P = new Point3D(CalibrationToken[p].x, CalibrationToken[p].y, CalibrationToken[p].z);
                        Point3D TP = CT.Transform(P);
                        M[p] = TP;
                        P1[p] = new Emgu.CV.Structure.MCvPoint3D32f(CalibrationToken[p].x, CalibrationToken[p].y, CalibrationToken[p].z);
                        P2[p] = new Emgu.CV.Structure.MCvPoint3D32f((float)TP.X, (float)TP.Y, (float)TP.Z);
                        //Helper3D.Add3DMarker(vCameras.Children,TP,0.1,BrushSet.GetBrush(p+1).Color);
                        }

                    Matrix<double> MD = null;
                    byte[] BT = null;
                    Emgu.CV.CvInvoke.CvEstimateAffine3D(P1, P2, out MD, out BT, 3, 0.99);
                    //Matrix3D MR=BuildTransformationMatrix(MD);

                    //MatrixTransform3D RT=new MatrixTransform3D(MR);

                    MatrixTransform3D RT = _ActiveWiimotes[i].GetIntrinsicPoseTransform(cbBaseCameraView.SelectedIndex);
                    }
                }

            if (ShowBaseTokenView)
                {
                for (int p = 0; p < TargetNumber; p++)
                    {
                    Point3D P = new Point3D(CalibrationToken[p].x, CalibrationToken[p].y, CalibrationToken[p].z);
                    //Point3D TPR=RT.Transform(M[p]);
                    Helper3D.Add3DMarker(vCameras.Children, P, 0.1, BrushSet.GetBrush(p + 1), true);
                    }
                }

            return;
            }

        private void ReconstructWiimotePositions()
            {
            if (ActiveWiimotes.Count == 0) return;
            bool IsComplete = false;
            CameraInfo RootWiimote = (cbRootWiimote.SelectedItem as CameraInfo);
            if (RootWiimote == null)
                RootWiimote = ActiveWiimotes[0];

            //foreach (CameraInfo Wii in AvailableWiimotes)
            //    {
            //    Wii.TransformMatrices = new List<Matrix3D>();
            //    Wii.PoseErrors = new List<double>();
            //    Wii.GlobalPoseTransform = null;
            //    }
            Matrix3D DM;
            if (cbBaseCameraView.SelectedIndex >= 0)
                DM = RootWiimote.GetTransformMatrix(cbBaseCameraView.SelectedIndex);
            else
                DM = RootWiimote.GetTransformMatrix(-5);

            foreach (CameraInfo SelectedCamera in AvailableWiimotes)
                    {
                    SelectedCamera.TransformMatrices.Clear();
                    SelectedCamera.PoseErrors.Clear();
                    SelectedCamera.GlobalPoseTransform = null;
                    }

            RootWiimote.GlobalPoseTransform = new MatrixTransform3D(Helper3D.RevertTransformationMatrix(DM));
            //DM=RevertTransformationMatrix(DM);
            //RootWiimote.GlobalPoseTransform = RootWiimote.GetIntrinsicPoseTransform();
            RootWiimote._GlobalPoseError = 0;
            // RootWiimote.GlobalPoseTransform=new MatrixTransform3D(new Matrix3D());// RootWiimote.GetStereoTransformMatrix(2));
            

            while (!IsComplete)
                {
                IsComplete = true;
                bool Deadlock = true;
                foreach (CameraInfo SelectedCamera in ActiveWiimotes)
                    {
                    if (SelectedCamera == RootWiimote) continue;
                    if (SelectedCamera.GlobalPoseTransform != null) continue;
                    IsComplete = false;

                    bool PoseIsComplete = true;
                  
                    SelectedCamera.TransformMatrices.Clear();
                    SelectedCamera.PoseErrors.Clear();

                    foreach (StereoCorrespondence SC in MeasuredStereoCorrespondences)
                        {
                        if (SC.Camera2 == SelectedCamera)
                            {
                            if (SC.Camera1.GlobalPoseTransform != null)
                                {
                                Matrix3D MF = SC.GetStereoTransformMatrix();
                                MF.Append(SC.Camera1._GlobalPoseTransform.Matrix);
                                SelectedCamera.TransformMatrices.Add(MF);
                                SelectedCamera.PoseErrors.Add(SC.Camera1._GlobalPoseError + SC._CorrespondenceError);
                                }
                            else
                                {
                                PoseIsComplete = false;
                                //SelectedCamera.TransformMatrices.Clear(); /// Not all oorrespondances are estimated.
                                //break;
                                }
                            }

                        if (SC.Camera1 == SelectedCamera) /// camera 1
                            {
                            if (SC.Camera2._GlobalPoseTransform != null) /// Root
                                {
                                Matrix3D MF = Helper3D.RevertTransformationMatrix(SC.GetStereoTransformMatrix());
                                MF.Append(SC.Camera2._GlobalPoseTransform.Matrix);
                                SelectedCamera.TransformMatrices.Add(MF);
                                SelectedCamera.PoseErrors.Add(SC.Camera2._GlobalPoseError + SC._CorrespondenceError);
                                }
                            else
                                {
                                PoseIsComplete = false;
                                }
                            }
                        }
                    if ((PoseIsComplete) && (SelectedCamera.TransformMatrices.Count > 0))
                    //if (SelectedCamera.TransformMatrices.Count!=0) /// all transforms are available
                        {
                        Deadlock = false;
                        SelectedCamera.GlobalPoseTransform = EstimateGlobalTransform(SelectedCamera);
                        SelectedCamera._GlobalPoseError = SelectedCamera.PoseErrors.Average();
                        //SelectedCamera.GlobalPoseTransform.Matrix.Invert();
                        }
                    }

                if ((Deadlock) && (!IsComplete))
                    {
                    int MaxTransform = 0;
                    CameraInfo MaxCamera = null;
                    foreach (CameraInfo Wii in ActiveWiimotes)
                        {
                        if (Wii == RootWiimote) continue;
                        if (Wii.GlobalPoseTransform != null) continue;
                        if (Wii.TransformMatrices.Count > MaxTransform)
                            {
                            MaxTransform = Wii.TransformMatrices.Count;
                            MaxCamera = Wii;
                            }
                        }
                    if (MaxCamera != null)
                        {
                        MaxCamera.GlobalPoseTransform = EstimateGlobalTransform(MaxCamera);
                        MaxCamera._GlobalPoseError = MaxCamera.PoseErrors.Average();
                        }
                    else
                        {
                        MessageBox.Show("The current set of stereo correspondences does not allow reconstruction of all wiimote positions!");
                        break;
                        }
                    }
                }
            }

        private MatrixTransform3D EstimateGlobalTransform(CameraInfo Wii)
            {
            Matrix3D M = new Matrix3D(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            for (int i = 0; i < Wii.TransformMatrices.Count; i++)
                {
                M.M11 = M.M11 + Wii.TransformMatrices[i].M11;
                M.M12 = M.M12 + Wii.TransformMatrices[i].M12;
                M.M13 = M.M13 + Wii.TransformMatrices[i].M13;
                M.M14 = M.M14 + Wii.TransformMatrices[i].M14;

                M.M21 = M.M21 + Wii.TransformMatrices[i].M21;
                M.M22 = M.M22 + Wii.TransformMatrices[i].M22;
                M.M23 = M.M23 + Wii.TransformMatrices[i].M23;
                M.M24 = M.M24 + Wii.TransformMatrices[i].M24;

                M.M31 = M.M31 + Wii.TransformMatrices[i].M31;
                M.M32 = M.M32 + Wii.TransformMatrices[i].M32;
                M.M33 = M.M33 + Wii.TransformMatrices[i].M33;
                M.M34 = M.M34 + Wii.TransformMatrices[i].M34;

                M.OffsetX = M.OffsetX + Wii.TransformMatrices[i].OffsetX;
                M.OffsetY = M.OffsetY + Wii.TransformMatrices[i].OffsetY;
                M.OffsetZ = M.OffsetZ + Wii.TransformMatrices[i].OffsetZ;
                M.M44 = M.M44 + Wii.TransformMatrices[i].M44;
                }

            //for (int i=0;i<SelectedCamera.TransformMatrices.Count;i++)
                {
                M.M11 = M.M11 / (double)Wii.TransformMatrices.Count;
                M.M12 = M.M12 / (double)Wii.TransformMatrices.Count;
                M.M13 = M.M13 / (double)Wii.TransformMatrices.Count;
                M.M14 = M.M14 / (double)Wii.TransformMatrices.Count;

                M.M21 = M.M21 / (double)Wii.TransformMatrices.Count;
                M.M22 = M.M22 / (double)Wii.TransformMatrices.Count;
                M.M23 = M.M23 / (double)Wii.TransformMatrices.Count;
                M.M24 = M.M24 / (double)Wii.TransformMatrices.Count;

                M.M31 = M.M31 / (double)Wii.TransformMatrices.Count;
                M.M32 = M.M32 / (double)Wii.TransformMatrices.Count;
                M.M33 = M.M33 / (double)Wii.TransformMatrices.Count;
                M.M34 = M.M34 / (double)Wii.TransformMatrices.Count;

                M.OffsetX = M.OffsetX / (double)Wii.TransformMatrices.Count;
                M.OffsetY = M.OffsetY / (double)Wii.TransformMatrices.Count;
                M.OffsetZ = M.OffsetZ / (double)Wii.TransformMatrices.Count;
                M.M44 = M.M44 / (double)Wii.TransformMatrices.Count;
                }

                return new MatrixTransform3D(M);
            }

        private void TransformRecursion(CameraInfo Parent, List<CameraInfo> Path)
            {
            foreach (StereoCorrespondence SC in MeasuredStereoCorrespondences)
                {
                if (SC.Camera1 == Parent)
                    {
                    List<CameraInfo> NewPath = new List<CameraInfo>(Path);
                    NewPath.Add(Parent);
                    TransformRecursion(SC.Camera1, NewPath);
                    }
                }
            }
        }
    }