﻿//<copyright
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods handling real-time tracking and 3D rendering of the results
// </copyright>
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Fluent;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow
        {
        [DllImport("inpout32.dll", EntryPoint = "Out32")]
        public static extern void Output(int adress, int value);

        [DllImport("inpout32.dll", EntryPoint = "Inp32")]
        public static extern int Input(int address);

        private void b2DTracking_Click(object sender, RoutedEventArgs e)
            {
            if (b2DTracking.Header.ToString() == "Start 2D")
                {
                Output(LPTPortNumber, 0);                    
                Thread.Sleep(2);
                Output(LPTPortNumber, 255);
                Thread.Sleep(5);
                Output(LPTPortNumber, 0); 
                Thread.Sleep(2);
                Output(LPTPortNumber, TriggerValue);

                b3DTracking.IsEnabled=false;
                b2DTracking.Header = "Stop";
                b2DTracking.LargeIcon = "Resources\\Icons\\StopTracking.png";
                BR = new SolidColorBrush(Colors.Red);
                BR.Freeze();
                b2DTracking.Background = BR;
                DT = new DispatcherTimer(DispatcherPriority.Normal);
                DT.Tick += new EventHandler(Tracking2DStep);
                /// Visualize EngagedWiimotes
                vTargets.Children.Clear();
                WriteLogLine("2D Tracking is started!");

                NewSamplesReady = false;
                LastUpdateTime = DateTime.Now;
                LogFileName = LogFolder + "\\" + DateTime.Now.ToString("yyyy-dd-MM HH_mm_ss") + "_2DTrackingSession.csv";
                try
                    {
                    LogFile = new System.IO.StreamWriter(LogFileName);
                    LogFile.Write(TriggerValue + "\n\r");
                    string Header = "Time,Counter,";
                    for (int i = 0; i < ActiveWiimotes.Count; i++)
                        {
                        Header = Header + "Wii " + i + " X1," + "Wii " + i + " Y1," + "Wii " + i + " X2," + "Wii " + i + " Y2," + "Wii " + i + " X3," + "Wii " + i + " Y3," + "Wii " + i + " X4," + "Wii " + i + " Y4,";
                        }
                    Header = Header + "\r\n";
                    LogFile.Write(Header);
                    }
                catch
                    {
                    LogFile = null;
                    WriteErrorLogLine("Unknown error on creating a log file!");
                    }
                IsLogFileEmpty = true;

                DT.Interval = TimeSpan.FromMilliseconds(0.05);
                StartTrackingTime = DateTime.Now;
                DT.Start();
                }
            else
                {
                Output(LPTPortNumber, 0);                    
                Thread.Sleep(2);
                Output(LPTPortNumber, 127); 


                b3DTracking.IsEnabled=true;
                if (DT != null)
                    {
                    DT.Stop();
                    DT.Tick -= Tracking3DTick;
                    }

                vTargets.Children.Clear();
                WriteLogLine("Tracking is stopped!");
                try
                    {
                    LogFile.Close();
                    LogFile.Dispose();

                    if (IsLogFileEmpty)
                        {
                        //stem.IO.File.Delete(LogFileName);
                        WriteLogLine("No data have been logged.");
                        }
                    else
                        {
                        WriteLogLine("The tracking data have been logged into the session file " + Path.GetFileName(LogFileName));
                        }
                    }
                catch
                    {
                    }

                b2DTracking.Header = "Start 2D";
                b2DTracking.LargeIcon = "Resources\\Icons\\StartTracking.png";
                BR = new SolidColorBrush(Colors.Green);
                BR.Freeze();
                BR = new SolidColorBrush(Colors.Red);
                }
            }

        private void Tracking2DStep(object sender, EventArgs e)
            {
            System.Windows.Forms.Application.DoEvents();

            TimeSpan TP = DateTime.Now - LastUpdateTime;
            
            TimeSpan GlobalTimer = DateTime.Now - StartTrackingTime;

            if ((RecordingTime>0)&&(GlobalTimer.TotalMilliseconds>RecordingTime))
                {
                if (DT != null)
                    {
                    DT.Stop();
                    DT.Tick -= Tracking3DTick;
                    }
                b2DTracking_Click(sender, null);
                return;
                }

            if (!NewSamplesReady)
                {
                return; /// nothing to update
                }
            if (System.Windows.Application.Current.MainWindow == null) return;
            //Stopwatch  SW=new Stopwatch();

           

            


            //_TrackingRate=1000.0/TP.TotalMilliseconds;
            double OldRate = _TrackingRate;
            _TrackingRate = (0.005 * (1000.0 / TP.TotalMilliseconds)) + (1.0 - 0.005) * _TrackingRate;
            if (double.IsInfinity(_TrackingRate))
                _TrackingRate = OldRate;
            LastUpdateTime = DateTime.Now;
            NewSamplesReady = false;
            //  SW.Start();
            string LogLine = Convert.ToInt64((DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds) + "," + GlobalTimer.TotalMilliseconds + ",";
            for (int i = 0; i < _ActiveWiimotes.Count; i++)
                {
                CameraInfo WI = _ActiveWiimotes[i];
                int CI = i;
                if (!WI._IsConnected) continue;

                System.Drawing.PointF[] Pos1 = WI.GetCurrentTarget2DPositions();// new System.Drawing.PointF[4];

                for (int p = 0; p < Pos1.Length; p++)
                    {
                    LogLine = LogLine + Pos1[p].X + "," + Pos1[p].Y + ",";
                    }
                }

            LogLine = LogLine + "\r\n";
            if ((LogFile != null) && (cLogging.IsChecked == true))
                {
                try
                    {
                    LogFile.Write(LogLine);
                    IsLogFileEmpty = false;
                    }
                catch
                { }
                }
            //if (!IsRenderingOn)
            
            }
        }
    }