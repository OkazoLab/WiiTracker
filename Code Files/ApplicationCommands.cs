﻿//<copyright
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods carrying the user-initiated commands in the application (e.g. after a button press)
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using System.Xml;
using Emgu.CV;
using Fluent;
using Microsoft.Win32;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow
        {
        #region Application Commands

        private void DeleteCommandHandler(object sender, ExecutedRoutedEventArgs e)
            {
            if (lbStereoCorrespondence.IsKeyboardFocusWithin)
                {
                cmRemoveStereoCorrespondence.Execute(null, null);
                }

            if (lbStates.IsKeyboardFocusWithin)
                {
                cmRemoveCalibrationViewset.Execute(null, null);
                }
            _IsApplicationCommandExecuting = false;
            }

        private void cmQuitApplicationExecute(object sender, ExecutedRoutedEventArgs e)
            {
            Close();
            _IsApplicationCommandExecuting = false;
            }

        private void RibbonWindow_Closing(object sender, CancelEventArgs e)
            {
            Properties.Settings.Default["DistortionWeight"] = DistortionWeight;
            Properties.Settings.Default["PoseWeight"] = PoseWeight;
            Properties.Settings.Default["BalanceWeight"] = BalanceWeight;
            Properties.Settings.Default["LogFolder"] = LogFolder;
            Properties.Settings.Default["IRSensitivity"] = IRSensitivity;


            Properties.Settings.Default["LPTPortNumber"] = LPTPortNumber;

            Properties.Settings.Default["TriggerValue"] = TriggerValue;
            Properties.Settings.Default["RecordingTime"] = RecordingTime;

            Properties.Settings.Default.Save();

            foreach (CameraInfo WI in _AvailableWiimotes)
                {
                WI._IsConnected = false;
                }
            }

        private void cmOpenConfigurationFileExecute(object sender, ExecutedRoutedEventArgs e)
            {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            openFileDialog1.Filter = "Calibration file (*.xml)|*.xml|All Files (*.*)|*.*";
            //openFileDialog1.FilterIndex = 1;

            if (openFileDialog1.ShowDialog() == true)
                {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SetupDescription), new Type[4] { typeof(CameraInfo), typeof(StereoCorrespondence), typeof(CameraView), typeof(CalibrationViewset) });
                //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(List<StereoCorrespondence>), new Type[2]{typeof(CameraInfo),typeof(StereoCorrespondence)});
                var FS = new FileStream(openFileDialog1.FileName, FileMode.Open);
                SetupDescription SD = (SetupDescription)x.Deserialize(FS);

                List<StereoCorrespondence> SerializedList = SD.StereoCorrespondences;
                List<CalibrationViewset> CS = SD.CSnapshots;
                List<CameraInfo> Wis = SD.AvailableWiimotes;
                WorldTranslationInLog=SD.WorldTranslationInLog;


                FS.Close();
                _LogEntries.Clear();
                WriteLogLine("Loading a setup...");
                ManageCameras(false, false);
                System.Windows.Forms.Application.DoEvents();

                BrushSet.RecreateColorSet();

                /// Update the currently available wiimotes with loaded parameters, e.g. calibrated camera matrix
                //List<int> UpdatedCameras = new List<int>();

                for (int l = 0; l < AvailableWiimotes.Count; l++)
                    {
                    //if (l >= Wis.Count) break;
                    //Wiimote W=AvailableWiimotes[l]._Device;
                    for (int i = 0; i < Wis.Count; i++)
                        {
                        if (AvailableWiimotes[l]._Index == Wis[i]._Index)
                            {
                            AvailableWiimotes[l] = Wis[i].Clone() as CameraInfo;
                            AvailableWiimotes[l].GlobalPoseTransform=null;
                            }
                        }

                    //AvailableWiimotes[l]._Device=W;
                    }

                ManageCameras(false, false);
                ReenumerateAvailableCameras();
                //ManageCameras(true, false);

                // ReenumerateAvailableCameras();

                ActiveWiimotes.Clear();
                foreach (CameraInfo WI in AvailableWiimotes)
                    ActiveWiimotes.Add(WI);

                MeasuredStereoCorrespondences.Clear();
                foreach (StereoCorrespondence SC in SerializedList)
                    {
                    SC.RestoreReferencies();
                    MeasuredStereoCorrespondences.Add(SC);
                    }

                CalibrationSnapshots.Clear();
                foreach (CalibrationViewset C in CS)
                    {
                    foreach (CameraView CV in C._CameraViews)
                        {
                        CV.RestoreCameraReference();
                        }
                    CalibrationSnapshots.Add(C);
                    }

                //for (int l = 0; l < AvailableWiimotes.Count; l++)
                //    {
                //    if (AvailableWiimotes[l]._IsConnected)
                //        {
                //        WriteLogLine("Turning on a camera..");
                //        try
                //            {
                //            AvailableWiimotes[l].Connect();
                //            }
                //        catch { }
                //        }
                //    }
                UpdateActiveWiimotes();
                WriteLogLine("A setup is loaded successfully!");
                }
            _IsApplicationCommandExecuting = false;
            }

        private void smSaveConfigurationFileExecute(object sender, ExecutedRoutedEventArgs e)
            {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Calibration file (*.xml)|*.xml|All Files (*.*)|*.*";
            //openFileDialog1.FilterIndex = 1;

            if (saveFileDialog1.ShowDialog() == true)
                {
                // System.Xml.Serialization.XmlSerializer x1 = new System.Xml.Serialization.XmlSerializer(typeof(List<StereoCorrespondence>), new Type[2]{typeof(CameraInfo),typeof(StereoCorrespondence)});
                SetupDescription SD = new SetupDescription();
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SetupDescription), new Type[5] { typeof(CameraInfo), typeof(StereoCorrespondence), typeof(CameraView), typeof(CalibrationViewset), typeof(IntrinsicCameraParameters) });

                var xmlTextWriter = XmlTextWriter.Create(saveFileDialog1.FileName, new XmlWriterSettings { Indent = true });//NewLineChars = "\r\n"});//, Indent = true });

                SD.StereoCorrespondences = new List<StereoCorrespondence>(MeasuredStereoCorrespondences);
                SD.CSnapshots = new List<CalibrationViewset>(CalibrationSnapshots);
                SD.AvailableWiimotes = new List<CameraInfo>(AvailableWiimotes);
                SD.WorldTranslationInLog=WorldTranslationInLog;

                x.Serialize(xmlTextWriter, SD);// MeasuredStereoCorrespondences[0]);

                xmlTextWriter.Close();

                //x.Serialize(xmlTextReader,SerializedList);
                }
            _IsApplicationCommandExecuting = false;
            }

        #endregion Application Commands

        #region Calibration Commands

        private void cmTakeCalibrationViewsetsetExecute(Object sender, ExecutedRoutedEventArgs e)
            {
            CalibrationViewset CS = new CalibrationViewset(this._ActiveWiimotes);
            if (CS.IsValidShot)
                {
                this.CalibrationSnapshots.Add(CS);
                tcViews.SelectedIndex = 1;
                lbStates.SelectedItem = CS;
                lbStates.ScrollIntoView(CS);
                CS.ResetIntrinsicCalibration();
                }
            else
                {
                this.CalibrationSnapshots.Add(CS);
                lbStates.SelectedItem = CS;
                lbStates.ScrollIntoView(CS);
                this.CalibrationSnapshots.Remove(CS);
                System.Console.Beep(250, 500);
                }
            _IsApplicationCommandExecuting = false;
            }

        private int TotalCalibrationViews = 100;
        private double ViewsetInterval = 0.3;
        private int CalibrationViews = 0;
        private int SelectedViewsets = 25;
        private DispatcherTimer CT;
        //List<CalibrationViewset> AutoViews;

        /// <summary>
        /// CaptorCameras list that include all active cameras and their pairs.
        /// </summary>
        private void BuildActiveCaptorsList()
            {
            CurrentViewsetCaptors.Clear();
            for (int i = 0; i < _ActiveWiimotes.Count; i++)
                {
                List<CameraInfo> L = new List<CameraInfo>();
                L.Add(_ActiveWiimotes[i]);
                CurrentViewsetCaptors.Insert(i, new ViewsetCaptor(L));
                for (int l = i + 1; l < _ActiveWiimotes.Count; l++)
                    {
                    L = new List<CameraInfo>();
                    L.Add(_ActiveWiimotes[i]);
                    L.Add(_ActiveWiimotes[l]);
                    CurrentViewsetCaptors.Add(new ViewsetCaptor(L));
                    }
                }
            }

        private void cmCollectViewsetStreamExecute(Object sender, ExecutedRoutedEventArgs e)
            {
            BuildActiveCaptorsList();
            dgCapturingViewSets W = new dgCapturingViewSets();
            W.DataContext = this;
            if (W.ShowDialog() == true)
                {
                W.DataContext = null;

                List<CalibrationViewset> SelectedAutoViews = new List<CalibrationViewset>();

                foreach (ViewsetCaptor VC in this.CurrentViewsetCaptors)
                    {
                    if (VC._IsAccepted)
                        {
                        int AcceptedViews = 0;

                        while (VC._RelatedViewsets.Count > 0 && AcceptedViews < VC._AcceptedCount)
                            {
                            //double MaxDistance=0;
                            //int MaxViewsetIndex=-1;

                            double MinX = double.MaxValue;
                            double MinY = double.MaxValue;
                            double MaxX = double.MinValue;
                            double MaxY = double.MinValue;

                            for (int i = 0; i < VC._RelatedViewsets.Count; i++)
                                {
                                double AX = 0;
                                double AY = 0;

                                //double Distance=0;
                                //if (VC._RelatedViewsets[i]._IsSelected) continue;

                                for (int l = 0; l < VC._RelatedViewsets[i]._CameraViews.Count; l++)
                                    {
                                    //for (int n=0;n<SelectedAutoViews[l]._CameraViews.Count;n++)
                                    //    {
                                    AX = AX + VC._RelatedViewsets[i]._CameraViews[l]._Quaternion[0];
                                    AY = AY + VC._RelatedViewsets[i]._CameraViews[l]._Quaternion[1];

                                    // Distance=Distance+Math.Abs(Helper3D.QuaternionDistance(VC._RelatedViewsets[i]._CameraViews[n]._Quaternion,SelectedAutoViews[l]._CameraViews[n]._Quaternion));
                                    //  }
                                    }
                                if (VC._RelatedViewsets[i]._CameraViews.Count > 0)
                                    {
                                    AX = AX / VC._RelatedViewsets[i]._CameraViews.Count;
                                    AY = AY / VC._RelatedViewsets[i]._CameraViews.Count;
                                    }

                                if (AX >= MaxX)
                                    MaxX = AX;
                                if (AY >= MaxY)
                                    MaxY = AY;

                                if (AX <= MinX)
                                    MinX = AX;
                                if (AY < MinY)
                                    MinY = AY;

                                //for (int l=0;l<VC._RelatedViewsets.Count;l++)
                                //    {
                                //    if (i==l) continue;
                                //    for (int n=0;n<VC._RelatedViewsets[i]._CameraViews.Count;n++)
                                //        {
                                //        Distance=Distance+Math.Abs(Helper3D.QuaternionDistance(VC._RelatedViewsets[i]._CameraViews[n]._Quaternion,VC._RelatedViewsets[l]._CameraViews[n]._Quaternion));
                                //        }

                                //    }

                                //for (int n=0;n<SelectedAutoViews.Count;n++)
                                //       {
                                //       //if (VC._RelatedViewsets[i]._CameraViews[n]._Quaternion[1]==0) continue;
                                //       //if (VC._RelatedViewsets[l]._CameraViews[n]._Quaternion[1]==0) continue;

                                //       Distance=Distance+Math.Abs(Helper3D.QuaternionDistance(VC._RelatedViewsets[i]._CameraViews[n]._Quaternion,VC._RelatedViewsets[l]._CameraViews[n]._Quaternion));
                                //       }

                                //if ((Distance>=MaxDistance)&&(!VC._RelatedViewsets[i]._IsSelected))
                                //    {
                                //    MaxDistance=Distance;
                                //    MaxViewsetIndex=i;
                                //    }
                                }

                            int Side = (int)Math.Sqrt(VC._AcceptedCount);
                            double StepX = (MaxX - MinX) / Side;
                            double StepY = (MaxY - MinY) / Side;

                            for (int x = 0; x < Side; x++)
                                {
                                for (int y = 0; y < Side; y++)
                                    {
                                    double cx = StepX * x + StepX / 2.0 + MinX;
                                    double cy = StepY * y + StepY / 2.0 + MinY;

                                    double MinDistance = double.MaxValue;
                                    int MinIndex = -1;
                                    for (int i = 0; i < VC._RelatedViewsets.Count; i++)
                                        {
                                        double AX = 0;
                                        double AY = 0;

                                        for (int l = 0; l < VC._RelatedViewsets[i]._CameraViews.Count; l++)
                                            {
                                            AX = AX + VC._RelatedViewsets[i]._CameraViews[l]._Quaternion[0];
                                            AY = AY + VC._RelatedViewsets[i]._CameraViews[l]._Quaternion[1];
                                            }
                                        if (VC._RelatedViewsets[i]._CameraViews.Count > 0)
                                            {
                                            AX = AX / VC._RelatedViewsets[i]._CameraViews.Count;
                                            AY = AY / VC._RelatedViewsets[i]._CameraViews.Count;
                                            }
                                        double Distance = Math.Sqrt(Math.Pow(AX - cx, 2) + Math.Pow(AY - cy, 2));
                                        if (Distance <= MinDistance)
                                            {
                                            MinDistance = Distance;
                                            MinIndex = i;
                                            }
                                        }

                                    if (MinIndex >= 0)
                                        {
                                        SelectedAutoViews.Add(VC._RelatedViewsets[MinIndex]);
                                        //VC._RelatedViewsets[MaxViewsetIndex]._IsSelected=true;
                                        VC._RelatedViewsets.RemoveAt(MinIndex);
                                        AcceptedViews++;
                                        }
                                    }
                                }

                            //for (int i=0;i<VC._RelatedViewsets.Count;i++)
                            //   {
                            //   }
                            }
                        }
                    }

                for (int i = 0; i < SelectedAutoViews.Count; i++)
                    {
                    //SelectedAutoViews[i].ResetIntrinsicCalibration();
                    this.CalibrationSnapshots.Add(SelectedAutoViews[i]);
                    }

                if (CalibrationSnapshots.Count > 0)
                    {
                    tcViews.SelectedIndex = this.CalibrationSnapshots.Count - 1;
                    lbStates.SelectedItem = this.CalibrationSnapshots[this.CalibrationSnapshots.Count - 1];
                    lbStates.ScrollIntoView(this.CalibrationSnapshots[this.CalibrationSnapshots.Count - 1]);
                    }
                }
            W.Owner = null;
            RebuildSelectedCameraViews();
            _IsApplicationCommandExecuting = false;

            return;
            /*
            CT = new DispatcherTimer();
            CT.Interval = new TimeSpan(0, 0, 0,0,Convert.ToInt32( 1000*ViewsetInterval));
            CT.Tick += DT_Tick;
            CalibrationViews = 0;
            CT.Start();
            AutoViews=new List<CalibrationViewset>();
            while (CT.IsEnabled)
                {
                System.Windows.Forms.Application.DoEvents();
                }

            /// Calculate

            double[] AvAreas=new double[AutoViews.Count];
            double MidArea=0;
            for (int i=0;i<AutoViews.Count;i++)
                {
                //if (AutoViews[i]._CameraViews.Count>1) continue;
                double[] CamViewArea=new double[AutoViews[i]._CameraViews.Count];
                AvAreas[i]=0;
                for (int v=0;v<AutoViews[i]._CameraViews.Count;v++)
                    {
                    for (int l=0;l<4;l++)
                        {
                        int in1=l;
                        int in2=l+1;
                        if (l==3) in2=0;
                        CamViewArea[v]=CamViewArea[v]+AutoViews[i]._CameraViews[v]._TargetPositions[in1].X*
                                                      AutoViews[i]._CameraViews[v]._TargetPositions[in2].Y-
                                                      AutoViews[i]._CameraViews[v]._TargetPositions[in1].Y*
                                                      AutoViews[i]._CameraViews[v]._TargetPositions[in2].X;
                        }
                    CamViewArea[v]=CamViewArea[v]/2.0;
                    AvAreas[i]=AvAreas[i]+CamViewArea[v];
                    }
                AvAreas[i]=AvAreas[i]/(1.0*AutoViews[i]._CameraViews.Count);
                MidArea=MidArea+AvAreas[i];
                }
            MidArea=MidArea/AutoViews.Count;

            List<CalibrationViewset> SelectedAutoViews=new List<CalibrationViewset>();
            List<double>Areas=new List<double>(AvAreas);

            while (SelectedAutoViews.Count<SelectedViewsets)
                {
                double MaxDistance=0;
                int MaxIndex=-1;
                if (Areas.Count==0) break;
                for (int i=0;i<Areas.Count;i++)
                    {
                    if (Math.Abs(MidArea-Areas[i])>MaxDistance)
                        {
                        MaxDistance=Math.Abs(MidArea-Areas[i]);
                        MaxIndex=i;
                        }
                    }

                SelectedAutoViews.Add(AutoViews[MaxIndex]);
                Areas.RemoveAt(MaxIndex);
                }

            for (int i=0;i<SelectedAutoViews.Count;i++)
                {
                SelectedAutoViews[i].ResetIntrinsicCalibration();
                this.CalibrationSnapshots.Add(SelectedAutoViews[i]);
                }

            tcViews.SelectedIndex = 1;
            lbStates.SelectedItem = this.CalibrationSnapshots[0];
            lbStates.ScrollIntoView(this.CalibrationSnapshots[0]);
            _IsApplicationCommandExecuting = false;
            */
            }

        private void cmRemoveCalibrationViewsetExecute(object sender, ExecutedRoutedEventArgs e)
            {
            for (int i = 0; i < lbStates.SelectedItems.Count; i++)
                {
                this.CalibrationSnapshots.Remove(lbStates.SelectedItems[i] as CalibrationViewset);
                i--;
                }
            if (lbStates.SelectedItem == null)
                {
                lbStates.SelectedIndex = 0;
                }
            _IsApplicationCommandExecuting = false;
            }

        private void cmResetCalibrationViewsetsExecute(object sender, ExecutedRoutedEventArgs e)
            {
            if (MessageBox.Show("Remove all calibration viewsets?", "Warning!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                this.CalibrationSnapshots.Clear();
                }
            _IsApplicationCommandExecuting = false;
            }

        async private void cmRecalibrateCameraExecute(object sender, ExecutedRoutedEventArgs e)
            {

            if (lbWiimotes.SelectedItem == null) { WriteErrorLogLine("No camera is selected"); _IsApplicationCommandExecuting = false; return; };
            if (!(lbWiimotes.SelectedItem as CameraInfo)._IsConnected) { WriteErrorLogLine("Selected camera is turn off"); _IsApplicationCommandExecuting = false; return; };
            WriteLogLine("Camera calibration is started. Please wait...");
            Mouse.OverrideCursor = Cursors.Wait;
            CameraInfo WI = (lbWiimotes.SelectedItem as CameraInfo as CameraInfo);
            await RunApplicationTask(() => RecalibrateSingleCamera(WI), true);
            WriteLogLine("Camera is recalibrated!");
            Mouse.OverrideCursor = null;
            _IsApplicationCommandExecuting = false;
            }

        #endregion Calibration Commands

        #region Stereo Correspondences

        private void cmRemoveStereoCorrespondenceExecute(object sender, ExecutedRoutedEventArgs e)
            {
            int i = lbStereoCorrespondence.SelectedIndex;
            while (lbStereoCorrespondence.SelectedItems.Count > 0)
                MeasuredStereoCorrespondences.Remove(lbStereoCorrespondence.SelectedItems[0] as StereoCorrespondence);
            if ((i > 0) && (i < MeasuredStereoCorrespondences.Count))
                lbStereoCorrespondence.SelectedIndex = i;
            else
                lbStereoCorrespondence.SelectedIndex = 0;
            _IsApplicationCommandExecuting = false;
            }

        async private void cmEstimateStereoCorrespondenceExecute(object sender, ExecutedRoutedEventArgs e)
            {
            CameraInfo WI1 = cbWiimoteA.SelectedItem as CameraInfo;
            CameraInfo WI2 = cbWiimoteB.SelectedItem as CameraInfo;
            if ((WI1 == null) || (WI2 == null) || (WI1 == WI2))
                {
                WriteErrorLogLine("Select two different cameras to start the estimation!");
                _IsApplicationCommandExecuting = false;
                return;
                }
            WriteLogLine("Estimation of stereo correspondence is started. Please wait...");
            Mouse.OverrideCursor = Cursors.Wait;
            int OldCount = MeasuredStereoCorrespondences.Count;
            await RunApplicationTask(() => EstimateStereoCorrespondence(WI1, WI2), true);

            if (OldCount == MeasuredStereoCorrespondences.Count)
                WriteLogLine("There are no calibration viewsets to estimate a stereo correspondence between " + WI1._Name + " and " + WI2._Name);
            Mouse.OverrideCursor = null;
            _IsApplicationCommandExecuting = false;
            WriteLogLine("Estimation of stereo correspondence is finished.");
            }

        async private void cmEstimateAllStereoCorrespondencesExecute(object sender, ExecutedRoutedEventArgs e)
            {
            WriteLogLine("Estimation of all stereo correspondences is started. Please wait...");
            for (int i = 0; i < _ActiveWiimotes.Count; i++)
                {
                for (int l = i + 1; l < _ActiveWiimotes.Count; l++)
                    {
                    if (i == l) continue;

                    bool IsNewCorrespondance = true;
                    foreach (StereoCorrespondence SC in MeasuredStereoCorrespondences)
                        {
                        if (((SC.Camera1 == _ActiveWiimotes[i]) && (SC.Camera2 == _ActiveWiimotes[l]))
                            ||
                            ((SC.Camera2 == _ActiveWiimotes[i]) && (SC.Camera1 == _ActiveWiimotes[l])))
                            {
                            IsNewCorrespondance = false;
                            break;
                            }
                        }
                    if (IsNewCorrespondance)
                        {
                        int OldCount = MeasuredStereoCorrespondences.Count;
                        await RunApplicationTask(() => EstimateStereoCorrespondence(_ActiveWiimotes[i], _ActiveWiimotes[l]), true);
                        if (OldCount < MeasuredStereoCorrespondences.Count)
                            {
                            WriteLogLine("Stereo correspondence between " + _ActiveWiimotes[i] + " and " + _ActiveWiimotes[l] + " is estimated");
                            }
                        else
                            WriteLogLine("There are no calibration viewsets to estimate a stereo correspondence between " + _ActiveWiimotes[i]._Name + " and " + _ActiveWiimotes[l]._Name);
                        }
                    }
                }
            WriteLogLine("Estimation of all stereo correspondences is finished.");
            _IsApplicationCommandExecuting = false;
            }

        #endregion Stereo Correspondences

        private void ManageCamera(CameraInfo WI, bool IsOn)
            {
            WI._IsConnected = IsOn;
            }

        private async void ManageCameras(bool IsOn, bool Async)
            {
            Mouse.OverrideCursor = Cursors.Wait;
            foreach (CameraInfo WI in AvailableWiimotes)
                {
                await RunApplicationTask(() => ManageCamera(WI, IsOn), Async);

                if (WI._IsConnected)
                    WriteLogLine(WI + " is ON");
                else
                    WriteLogLine(WI + " is OFF");
                if (!Async) System.Windows.Forms.Application.DoEvents();
                }
            UpdateActiveWiimotes();
            Mouse.OverrideCursor = null;
            _IsApplicationCommandExecuting = false;
            }

        private void cmManageCamerasExecute(object sender, ExecutedRoutedEventArgs e)
            {
            bool On = Boolean.Parse(e.Parameter.ToString());
            ManageCameras(On, true);
            }

        async private void cmManageCameraExecute(object sender, ExecutedRoutedEventArgs e)
            {
            System.Windows.Controls.Primitives.ToggleButton TB = (e.Parameter as System.Windows.Controls.Primitives.ToggleButton);
            CameraInfo WI = (TB.DataContext as CameraInfo);
            Mouse.OverrideCursor = Cursors.Wait;
            bool IsOn = (bool)TB.IsChecked;
            await RunApplicationTask(() => ManageCamera(WI, IsOn), true);
            if (WI._IsConnected)
                WriteLogLine(WI + " is ON");
            else
                if (IsOn)
                    WriteErrorLogLine("Error while turning on " + WI + "!");
                else
                    WriteLogLine(WI + " is OFF");
            UpdateActiveWiimotes();
            Mouse.OverrideCursor = null;
            _IsApplicationCommandExecuting = false;
            }


        Matrix3D WorldTranslationInLog=Matrix3D.Identity;

        private void cmEstimate3DWorldTranslationExecute(object sender, ExecutedRoutedEventArgs e)
            {
            dgWorldTranslation WT=new dgWorldTranslation();
            if ((bool)WT.ShowDialog())
                {
                WorldTranslationInLog=WT.ResultMatrix;
                }
            _IsApplicationCommandExecuting = false;
            }
        }
    }