﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods handling real-time tracking and 3D rendering of the results
// </copyright>
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using Emgu.CV;
using Fluent;
using WiimoteLib;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow
        {
        private DispatcherTimer DT;
        public bool NewSamplesReady;
        DateTime LastUpdateTime;
        DateTime StartTrackingTime;
        SolidColorBrush BR;
        System.IO.StreamWriter LogFile;
        string LogFileName="";
        bool IsLogFileEmpty;

        /// <summary>
        /// Runs or stop 3D tracking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void b3DTracking_Click(object sender, RoutedEventArgs e)
            {
            if (b3DTracking.Header.ToString() == "Start 3D")
                {
                Output(LPTPortNumber, 0);                    
                Thread.Sleep(2);
                Output(LPTPortNumber, 255); 
                b2DTracking.IsEnabled=false;
                bool NeedToReconstruct=false;
                for (int i=0;i<_ActiveWiimotes.Count;i++)
                    {
                    if (_ActiveWiimotes[i].GlobalPoseTransform==null)
                        {
                        NeedToReconstruct=true;
                        break;
                        }
                    }
                if (NeedToReconstruct)
                     ShowCameras(false);
                
                for (int i=0;i<_ActiveWiimotes.Count;i++)
                    {
                    CameraInfo CI=_ActiveWiimotes[i];
                    if (CI.IP==null)
                        {
                        WriteErrorLogLine("Tracking is not possible because at least one active camera is not calibrated yet!");
                        return;
                        }
                    }

                b3DTracking.Header = "Stop";
                b3DTracking.LargeIcon="Resources\\Icons\\StopTracking.png";
                BR=new SolidColorBrush(Colors.Red);
                BR.Freeze();
                b3DTracking.Background=BR;
                DT = new DispatcherTimer(DispatcherPriority.Normal);
                DT.Tick += new EventHandler(Tracking3DTick);
                /// Visualize EngagedWiimotes
                vTargets.Children.Clear();
                WriteLogLine("Tracking is started!");
                
                NewSamplesReady=false;
                LastUpdateTime=DateTime.Now;
                LogFileName=LogFolder+"\\"+DateTime.Now.ToString("yyyy-dd-MM HH_mm_ss")+"_TrackingSession.csv";
                try{
                    LogFile = new System.IO.StreamWriter(LogFileName);
                    LogFile.Write("Time,Counter,X1,Y1,Z1,X2,Y2,Z2,X3,Y3,Z3,X4,Y4,Z4\r\n");
                    }
                    catch
                    {
                    LogFile = null;
                    WriteErrorLogLine("Unknown error on creating a log file!");
                    }
                IsLogFileEmpty=true;

                DT.Interval = TimeSpan.FromMilliseconds(0.05);
                StartTrackingTime=DateTime.Now;
                DT.Start();
                }
            else
                {
                b2DTracking.IsEnabled=true;
                Output(LPTPortNumber, 0);                    
                Thread.Sleep(2);
                Output(LPTPortNumber, 127); 
                if (DT != null)
                    {
                    DT.Stop();
                    DT.Tick -= Tracking3DTick;
                    }

                vTargets.Children.Clear();
                WriteLogLine("Tracking is stopped!");
                try{
                    LogFile.Close();
                    LogFile.Dispose();

                    if (IsLogFileEmpty)
                        {
                        System.IO.File.Delete(LogFileName);
                        WriteLogLine("No data have been logged.");
                        }
                        else
                        {
                        WriteLogLine("The tracking data have been logged into the session file "+Path.GetFileName(LogFileName));
                        }
                    }
                    catch
                    {
                    }
                
                b3DTracking.Header = "Start 3D";
                b3DTracking.LargeIcon="Resources\\Icons\\StartTracking.png";
                 BR=new SolidColorBrush(Colors.Green);
                BR.Freeze();
                BR=new SolidColorBrush(Colors.Red);
                }
            }

        private void Tracking3DTick(object sender, EventArgs e)
            {            
            if (!NewSamplesReady) return; /// nothing to update
            if (Application.Current.MainWindow==null) return;
            //Stopwatch  SW=new Stopwatch();
                                     
            TimeSpan TP=DateTime.Now-LastUpdateTime;
            //Console.WriteLine("UT="+TP.TotalMilliseconds);
            TimeSpan GlobalTimer=DateTime.Now-StartTrackingTime;
            //_TrackingRate=1000.0/TP.TotalMilliseconds;
            double OldRate=_TrackingRate;
            _TrackingRate = (0.005 * (1000.0/TP.TotalMilliseconds)) + (1.0 - 0.005) * _TrackingRate;
            if (double.IsInfinity(_TrackingRate))
                _TrackingRate =OldRate;
            LastUpdateTime=DateTime.Now;
            NewSamplesReady=false;
          

           //return;
            bool IsRenderingOn=(tcViews.SelectedItem==ti3DSpace);//.Visibility==Visibility.Visible);// tcViews.SelectedIndex==0;   /// 3D scene is not visible           
            //return;
            if (IsRenderingOn)
                {
                vTargets.Children.Clear();
                DT.Interval=new TimeSpan(0, 0, 0, 0, 50);;
                }
                else
                DT.Interval=TimeSpan.FromMilliseconds(0);

            CameraInfo WI;
            List<List<Point3D>> EstimatedTargets = new List<List<Point3D>>();

            Point3D[,] RayOrigins =new Point3D[_AvailableWiimotes.Count,TargetNumber]; 
            
            Vector3D[,] RayVectors= new Vector3D[_AvailableWiimotes.Count,TargetNumber]; 
           
            for (int i=0;i<TargetNumber;i++)
                {
                EstimatedTargets.Add(new List<Point3D>());
                }


            for (int i=0;i<_ActiveWiimotes.Count;i++)
                {
                WI=_ActiveWiimotes[i];
                int CI=i;
                if (!WI._IsConnected) continue;
                
                System.Drawing.PointF[] Pos1 = WI.GetCurrentTarget2DPositions();// new System.Drawing.PointF[4];
                if (WI.IP==null) continue;


                System.Drawing.PointF[] UndistortedPoints = WI.IP.Undistort(Pos1, null, null);
                for (int p = 0; p < UndistortedPoints.Length; p++)
                        {
                        UndistortedPoints[p].X = (float)(UndistortedPoints[p].X * WI.IP.IntrinsicMatrix[0, 0] + WI.IP.IntrinsicMatrix[0, 2]);
                        UndistortedPoints[p].Y = (float)(UndistortedPoints[p].Y * WI.IP.IntrinsicMatrix[1, 1] + WI.IP.IntrinsicMatrix[1, 2]);
                        }

                MatrixTransform3D M;
                if (WI._GlobalPoseTransform != null)
                    M = WI._GlobalPoseTransform;
                    else
                    M = WI.GetIntrinsicPoseTransform(cbBaseCameraView.SelectedIndex);

                 for (int t=0;t<TargetNumber;t++)
                    {
                    if (!_IsTargetTracked[t]) continue;

                    Point3D CenterIm = new Point3D(0, 0, 0);
                    CenterIm.X = (UndistortedPoints[t].X - WI.IP.IntrinsicMatrix[0, 2]) / (1024 / 1.5);//(-1.0/0.00146484375);
                    CenterIm.Y = (UndistortedPoints[t].Y - WI.IP.IntrinsicMatrix[1, 2]) / (768 / 1.125);///(-1.0/0.00146484375);
                    double DiagonalInPixels=Math.Sqrt(1024*1024+768*768);  
                    double CenterProximityInPixels=Math.Sqrt(
                        Math.Pow(UndistortedPoints[t].X - WI.IP.IntrinsicMatrix[0, 2],2)+
                        Math.Pow(UndistortedPoints[t].Y - WI.IP.IntrinsicMatrix[1, 2],2));                              
                    WI.NormalizedTargetProximity[t]=CenterProximityInPixels/(DiagonalInPixels/2.0);
                    
                     
                     
                                                                                                        /// Point3D PS = M.Transform(CenterIm);
                    CenterIm=M.Transform(CenterIm);

                    if (!CenterIm.IsValid()) continue;
               
                    

                    if ((Show2DTargetProjections)&&(IsRenderingOn))
                        Helper3D.Add3DMarker(vTargets.Children, CenterIm, 0.1, BrushSet.GetBrush(t ),true);

                      double f = (WI.IP.IntrinsicMatrix[0, 0] / 1024.0 * 1.5 + WI.IP.IntrinsicMatrix[1, 1] / 768.0 * 1.125) / 2.0; // in mm
                        RayOrigins[CI,t]= new Point3D(0, 0, -f);
                        RayOrigins[CI,t]=M.Transform(RayOrigins[CI,t]);
                        Point3D RayEndPoint=new Point3D();

                        RayEndPoint.X = RayOrigins[CI,t].X + 15 * (CenterIm.X - RayOrigins[CI,t].X);
                        RayEndPoint.Y = RayOrigins[CI,t].Y + 15 * (CenterIm.Y - RayOrigins[CI,t].Y);
                        RayEndPoint.Z = RayOrigins[CI,t].Z + 15 * (CenterIm.Z - RayOrigins[CI,t].Z);



                    if ((ShowProjectionRays)&&(IsRenderingOn))
                        {
                        HelixToolkit.Wpf.LinesVisual3D LV1 = new HelixToolkit.Wpf.LinesVisual3D();
                        LV1.Points.Add(RayOrigins[CI,t]);
                        LV1.Points.Add(RayEndPoint);
                        LV1.Thickness = 2;
                        LV1.Color = (BrushSet.GetBrush(t) as SolidColorBrush).Color;

                        
                            vTargets.Children.Add(LV1);
                        }
                    RayVectors[CI,t] = new Vector3D(RayEndPoint.X - RayOrigins[CI,t].X, RayEndPoint.Y - RayOrigins[CI,t].Y, RayEndPoint.Z - RayOrigins[CI,t].Z);
                    }   
                }

           

              for (int i=0;i<_ActiveWiimotes.Count;i++)
                {
                for (int l=i+1;l<_ActiveWiimotes.Count;l++)
                {
                    //for (int c = 0; c < MeasuredStereoCorrespondences.Count; c++)
                    //    {
                        //StereoCorrespondence CP = MeasuredStereoCorrespondences[c];
                        if ((!_ActiveWiimotes[i]._IsConnected) || (!_ActiveWiimotes[l]._IsConnected)) continue;

                        int CI1=i;
                        int CI2=l;

                         for (int t=0;t<TargetNumber;t++)
                            {

                            if (!_IsTargetTracked[t]) continue;
                            if (RayVectors[CI1,t].Length==0) continue;
                            if (RayVectors[CI2,t].Length==0) continue;

                            Point3D R1, R2;
                            Point3D CR = Helper3D.FindBestIntersection(RayOrigins[CI1,t], RayVectors[CI1,t], RayOrigins[CI2,t], RayVectors[CI2,t], out R1, out R2);


                            double DX=R2.X-R1.X;
                            double DY=R2.Y-R1.Y;
                            double DZ=R2.Z-R1.Z;

                            double Camera1ProximityWeight=_ActiveWiimotes[i].NormalizedTargetProximity[t]/(_ActiveWiimotes[i].NormalizedTargetProximity[t]+_ActiveWiimotes[l].NormalizedTargetProximity[t]);
                            double ProximityRatio=this._DistortionWeight*Camera1ProximityWeight+(1-this._DistortionWeight)*0.5;


                            double Camera1PoseWeight=_ActiveWiimotes[i]._GlobalPoseError/(_ActiveWiimotes[i]._GlobalPoseError+_ActiveWiimotes[l]._GlobalPoseError);
                            double PoseRatio=this._PoseWeight*Camera1PoseWeight+(1-this._PoseWeight)*0.5;

                            double CombinedRatio=(1-_BalanceWeight)*PoseRatio+this._BalanceWeight*ProximityRatio;

                            double MX=R1.X+CombinedRatio*DX;
                            double MY=R1.Y+CombinedRatio*DY;
                            double MZ=R1.Z+CombinedRatio*DZ;

                            Point3D tTarget = new Point3D(MX,MY,MZ);

                            //Point3D tTarget = new Point3D((R1.X + R2.X) / 2.0, (R1.Y + R2.Y) / 2.0, (R1.Z + R2.Z) / 2.0);
                            if (!tTarget.IsValid()) continue;
                            if (tTarget.IsZero()) continue;

               
                            EstimatedTargets[t].Add(tTarget);

                            if ((ShowRayInterSections)&&(IsRenderingOn))
                                {
                                HelixToolkit.Wpf.LinesVisual3D ShortestLine = new HelixToolkit.Wpf.LinesVisual3D();
                                ShortestLine.Points.Add(R1);
                                ShortestLine.Points.Add(R2);
                                ShortestLine.Thickness =2;
                                Color D= (BrushSet.GetBrush(t) as SolidColorBrush).Color;
                                D.A=Convert.ToByte(D.A/2);
                               
                                ShortestLine.Color = D;
                               
                                
                                vTargets.Children.Add(ShortestLine);
                                Helper3D.Add3DMarker(vTargets.Children, tTarget, 0.05, BrushSet.GetBrush(t+1),true);
                                }
                            }
                      //  }
                    }
                }
            
           //  SW.Start();   
            string LogLine=Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds)+","+GlobalTimer.TotalMilliseconds+",";
            
            for (int i=0;i<TargetNumber;i++)
                {   
                if (!((!_IsTargetTracked[i])||(EstimatedTargets[i].Count==0))) 
                    {    
                    Point3D MTarget = new Point3D(0, 0, 0);

                    for (int ii = 0; ii < EstimatedTargets[i].Count; ii++)
                        {
                        MTarget.Offset(EstimatedTargets[i][ii].X, EstimatedTargets[i][ii].Y, EstimatedTargets[i][ii].Z);
                        }

                    MTarget.X = MTarget.X / (1.0 * EstimatedTargets[i].Count);
                    MTarget.Y = MTarget.Y / (1.0 * EstimatedTargets[i].Count);
                    MTarget.Z = MTarget.Z / (1.0 * EstimatedTargets[i].Count);
                    double SD=0;
                    for (int ii = 0; ii < EstimatedTargets[i].Count; ii++)
                        {
                        double Distance=(Math.Pow(EstimatedTargets[i][ii].X-MTarget.X,2)+
                                Math.Pow(EstimatedTargets[i][ii].Y-MTarget.Y,2)+
                                Math.Pow(EstimatedTargets[i][ii].Z-MTarget.Z,2));

                        SD=SD+Distance;
                        }
                    SD=Math.Sqrt(SD);


                    //MTarget=WorldTranslationInLog.Transform(MTarget);
                    TrackedTargets[i].CaptorNumber=EstimatedTargets[i].Count;
                    TrackedTargets[i].UncertantyScore=SD;


                    if (IsRenderingOn)
                        Helper3D.Add3DMarker(vTargets.Children, MTarget, 0.2, BrushSet.GetBrush(i),false);
                    TrackedTargets[i]._TargetPosition=MTarget;
                    //MTarget=WorldTranslationInLog.Transform(MTarget);
                    LogLine=LogLine+MTarget.X.ToString(CultureInfo.InvariantCulture)+","+MTarget.Y.ToString(CultureInfo.InvariantCulture)+","+MTarget.Z.ToString(CultureInfo.InvariantCulture)+",";
                    }
                    else
                    {
                    TrackedTargets[i]._TargetPosition=new Point3D(0,0,0);
                    LogLine=LogLine+","+","+",";
                    }
                }
            if (Application.Current.MainWindow!=null)
                (Application.Current.MainWindow as MainWindow).OnPropertyChanged("_RulerDistance");
          
           //Console.WriteLine("Time elapsed: {0}",SW.Elapsed.TotalMilliseconds);
            LogLine=LogLine+"\r\n";
            if ((LogFile!=null)&&(cLogging.IsChecked==true))
                {  
                try
                    {
                    LogFile.Write(LogLine);
                    IsLogFileEmpty=false;
                    }
                catch 
                    {}
                }
               
           
        
           if (!IsRenderingOn)
               System.Windows.Forms.Application.DoEvents();

            }
        }
    }