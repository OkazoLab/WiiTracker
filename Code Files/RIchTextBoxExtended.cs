﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a extended control for the introduction and help document shown in the application menu
// </copyright>
      
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;

namespace WiiTracker
    {
   
	/// <summary>
	/// Represents an extended <see cref="RichTextBox"/> control.
	/// </summary>
	public class RichTextBoxExtended : RichTextBox {

        private MemoryStream	previewStream;

		#region Dependency Properties
		
		/// <summary>
		/// Identifies the <see cref="DocumentUri"/> dependency property.  This field is read-only.
		/// </summary>
		/// <value>The identifier for the <see cref="DocumentUri"/> dependency property.</value>
		public static readonly DependencyProperty DocumentUriProperty = DependencyProperty.Register("DocumentUri", typeof(Uri), typeof(RichTextBoxExtended), new FrameworkPropertyMetadata(null, OnDocumentUriPropertyValueChanged));
		
        /// <summary>
		/// Occurs when the <see cref="DocumentUriProperty"/> value is changed.
		/// </summary>
		/// <param name="obj">The <see cref="DependencyObject"/> whose property is changed.</param>
		/// <param name="e">A <see cref="DependencyPropertyChangedEventArgs"/> that contains the event data.</param>
		private static void OnDocumentUriPropertyValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e) {
			RichTextBoxExtended control = (RichTextBoxExtended)obj;
			try {
				control.Document = Application.LoadComponent(control.DocumentUri) as FlowDocument;
			}
			catch {}
		}
        
         public static string GetDocumentRTF(DependencyObject obj) 
              {
                return (string)obj.GetValue(DocumentRTFProperty); 
              }
          public static void SetDocumentRTF(DependencyObject obj, string value) 
          {
            obj.SetValue(DocumentRTFProperty, value); 
          }

      public static readonly DependencyProperty DocumentRTFProperty = 
        DependencyProperty.RegisterAttached(
          "DocumentRTF",
          typeof(string),
          typeof(RichTextBoxExtended),
          new FrameworkPropertyMetadata(null,FrameworkPropertyMetadataOptions.AffectsArrange)
          {
            BindsTwoWayByDefault = true,
            PropertyChangedCallback = (obj, e) =>
            {
              
              var richTextBox = (RichTextBoxExtended)obj;
              if (richTextBox.BindingChange) return;
              // Parse the XAML to a document (or use XamlReader.Parse())
              var xaml = GetDocumentRTF(richTextBox);
              if (xaml==null) 
                  xaml="{}";               

              var doc = new FlowDocument();
              var range = new TextRange(doc.ContentStart, doc.ContentEnd);              
              range.Load(new MemoryStream(Encoding.UTF8.GetBytes(xaml)), 
                DataFormats.Rtf);                              
              // Set the document
              
                
                richTextBox.BindingChange=true;
                richTextBox.Document = doc;
                richTextBox.Document.PagePadding=richTextBox.Padding;
                richTextBox.BindingChange=false;
                

              // When the document changes update the source
              //richTextBox.TextChanged += (obj2, e2) =>
              //  {
              //      RichTextBox richTextBox2 = obj2 as RichTextBox;
              //      if (richTextBox2 != null)
              //          {
              //          TextRange tr = new TextRange(richTextBox.Document.ContentStart,
              //          richTextBox.Document.ContentEnd);
              //          MemoryStream ms = new MemoryStream();
              //          tr.Save(ms, DataFormats.Rtf);
              //          SetDocumentXaml(richTextBox,Encoding.UTF8.GetString(ms.ToArray()));
              //          }
              //  };

              //range.Changed += (obj2, e2) =>
              //{
              //  if(richTextBox.Document==doc)
              //  {
              //    MemoryStream buffer = new MemoryStream();
              //    range.Save(buffer, DataFormats.Rtf);
              //    SetDocumentXaml(richTextBox, 
              //      Encoding.UTF8.GetString(buffer.ToArray()));
              //  }
              //};
           }});       
               


		#endregion
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes the <c>RichTextBoxExtended</c> class.
		/// </summary>
		static RichTextBoxExtended() {
			AcceptsReturnProperty.OverrideMetadata(typeof(RichTextBoxExtended), new FrameworkPropertyMetadata(true));
			AcceptsTabProperty.OverrideMetadata(typeof(RichTextBoxExtended), new FrameworkPropertyMetadata(true));
			HorizontalScrollBarVisibilityProperty.OverrideMetadata(typeof(RichTextBoxExtended), new FrameworkPropertyMetadata(ScrollBarVisibility.Hidden));
			VerticalScrollBarVisibilityProperty.OverrideMetadata(typeof(RichTextBoxExtended), new FrameworkPropertyMetadata(ScrollBarVisibility.Hidden));
		}

		/// <summary>
		/// Initializes an instance of the <c>RichTextBoxExtended</c> class.
		/// </summary>
		public RichTextBoxExtended() {
			// Load content
            //this.Style = this.TryFindResource(SharedResourceKeys.TextBoxBaseStyleKey) as Style;
            //this.BorderBrush = Brushes.Black;
            //this.BorderThickness = new Thickness(1);
            
			

			// Add command bindings
       

            TextChanged += new TextChangedEventHandler(RichTextBoxExtended_TextChanged);
		}

        public bool BindingChange=false;

        void RichTextBoxExtended_TextChanged(object sender, TextChangedEventArgs e)             
            {  
            //return;           
            if (BindingChange== false)
                {
                BindingChange=true;
                TextRange tr = new TextRange(this.Document.ContentStart,
                this.Document.ContentEnd);
                MemoryStream ms = new MemoryStream();
                tr.Save(ms, DataFormats.Rtf);
                SetDocumentRTF(this,Encoding.UTF8.GetString(ms.ToArray()));
                this.Width=10;
                
                BindingChange=false;
                }       
            }
				
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/// <summary>
		/// Activates preview mode.
		/// </summary>
		public void ActivatePreviewMode() {
			if (previewStream == null) {
				if (this.Selection.IsEmpty) {
					// When the selection is empty, we need to select something for the preview stream functionality to work correctly
					if (this.Selection.End != this.Selection.End.DocumentEnd)
						EditingCommands.SelectRightByCharacter.Execute(null, this);
					else if (this.Selection.Start != this.Selection.Start.DocumentStart)
						EditingCommands.SelectRightByCharacter.Execute(null, this);
				}

                previewStream = new MemoryStream();
	            this.Selection.Save(previewStream, DataFormats.Xaml);
			}
		}

		/// <summary>
		/// Deactivates preview mode.
		/// </summary>
		/// <param name="restoreOldSettings">Whether to restore the old settings.</param>
		public void DeactivatePreviewMode(bool restoreOldSettings) {
			if (previewStream != null) {
				if (restoreOldSettings)
					this.Selection.Load(previewStream, DataFormats.Xaml);
				previewStream.Dispose();
				previewStream = null;
			}
		}
		
		/// <summary>
		/// Gets or sets a <see cref="Uri"/> indicating the location of the <see cref="FlowDocument"/> to load.
		/// </summary>
		/// <value>A <see cref="Uri"/> indicating the location of the <see cref="FlowDocument"/> to load.</value>
		public Uri DocumentUri {
			get {
				return (Uri)this.GetValue(RichTextBoxExtended.DocumentUriProperty);
			}
			set {
				this.SetValue(RichTextBoxExtended.DocumentUriProperty, value);
			}
		}
		
		/// <summary>
		/// Loads the document text.
		/// </summary>
		/// <param name="text">The text to load.</param>
		public void LoadDocument(string text) {
			MemoryStream stream = new MemoryStream();
			StreamWriter writer = new StreamWriter(stream);
			writer.Write(text);
			writer.Flush();
			stream.Position = 0;
			TextRange range = new TextRange(this.Document.ContentStart, this.Document.ContentEnd);
            range.Load(stream, DataFormats.Rtf);
			stream.Close();
		}

		/// <summary>
		/// Gets whether preview mode is active.
		/// </summary>
		/// <value>
		/// <c>true</c> if preview mode is active; otherwise, <c>false</c>.
		/// </value>
		public bool IsPreviewModeActive {
			get {
				return (previewStream != null);
			}
		}

		/// <summary>
		/// Invoked when an unhandled <see cref="UIElement.MouseUp"/> routed event is raised on this element. 
		/// Implement this method to add class handling for this event. 
		/// </summary>
		/// <param name="e">The event data for the <see cref="UIElement.MouseUp"/> event.</param>
		protected override void OnMouseUp(MouseButtonEventArgs e) {
			// Call the base method
			base.OnMouseUp(e);

			// If a selection was just made with the mouse and we are not in an XBAP...
            //if ((e.ChangedButton == MouseButton.Left) && (!this.Selection.IsEmpty) && (!BrowserInteropHelper.IsBrowserHosted)) {
            //    // Show the mini-toolbar
            //    MiniToolBarService.Show(new ActiproSoftware.ProductSamples.RibbonSamples.Common.RichTextBoxMiniToolBar(), 
            //        this, e.GetPosition(this));
            //}
		}
		
		/// <summary>
		/// Called when the rendered size of a control changes. 
		/// </summary>
		/// <param name="sizeInfo">Specifies the size changes.</param>
		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
			// Call the base method
			base.OnRenderSizeChanged(sizeInfo);

			// Adjust the document's page width (since there is a WPF bug when used within a parent ScrollViewer with horizontal scroll capabilities)
			if (this.Document != null)
				this.Document.PageWidth = this.ActualWidth - this.BorderThickness.Left - this.Padding.Left - this.BorderThickness.Right - this.Padding.Right;
		}

	
		
		
		
		

	}
    }
