﻿//<copyright 
/// Okazolab (www.okazolab.com)
/// Copyright (c) 2012 All Rights Reserved
// <author>
///Ilia Korjoukov
//</author>
/// This code file contains a group of methods serving the Main Window GUI in the application 
// </copyright>


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Fluent;
using WiimoteLib;

namespace WiiTracker
    {
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
        {
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
            {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                {
                handler(this, new PropertyChangedEventArgs(name));
                }
            }

        #region Application Command Definitions

        public static RoutedCommand cmTakeCalibrationViewset = new RoutedCommand("Take Calibration Viewset", typeof(MainWindow));

        public static RoutedCommand cmCollectViewsetStream= new RoutedCommand("Collect Calibration Stream", typeof(MainWindow));
        public static RoutedCommand cmRemoveCalibrationViewset = new RoutedCommand("Remove Calibration Viewset", typeof(MainWindow));
        public static RoutedCommand cmResetCalibrationViewsets = new RoutedCommand("Reset Calibration Viewsets", typeof(MainWindow));

        public static RoutedCommand cmRecalibrateCamera = new RoutedCommand("Recalibrate Camera", typeof(MainWindow));

        public static RoutedCommand cmEstimateStereoCorrespondence = new RoutedCommand("Estimate Stereo", typeof(MainWindow));
        public static RoutedCommand cmEstimateAllStereoCorrespondences = new RoutedCommand("Estimate all stereo", typeof(MainWindow));

        public static RoutedCommand cmRemoveStereoCorrespondence = new RoutedCommand("Remove Stereo Correspondence", typeof(MainWindow));

        public static RoutedCommand cmManageCameras = new RoutedCommand("Manage Cameras", typeof(MainWindow));
        public static RoutedCommand cmManageCamera = new RoutedCommand("Manage Camera", typeof(MainWindow));


        public static RoutedCommand cmEstimate3DWorldTranslation = new RoutedCommand("World translation", typeof(MainWindow));
        public static RoutedCommand cmCaptureRulerOrigin = new RoutedCommand("Capture Ruler Origin", typeof(MainWindow));
        
        #endregion Application Command Definitions

        public MainWindow()
            {
            InitializeComponent();
            EnumerateAvailableCameras();
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Close, new ExecutedRoutedEventHandler(cmQuitApplicationExecute)));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.SaveAs, new ExecutedRoutedEventHandler(smSaveConfigurationFileExecute)));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Open, new ExecutedRoutedEventHandler(cmOpenConfigurationFileExecute)));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, new ExecutedRoutedEventHandler(DeleteCommandHandler)));
            _LogEntries = new AsyncObservableCollection<LogEntry>();

            WriteLogLine("Started successfully. " + AvailableWiimotes.Count + " cameras are found in the system.");

            CommandBindings.Add(new CommandBinding(cmTakeCalibrationViewset, new ExecutedRoutedEventHandler(cmTakeCalibrationViewsetsetExecute)));
           CommandBindings.Add(new CommandBinding(cmCollectViewsetStream, new ExecutedRoutedEventHandler(cmCollectViewsetStreamExecute)));
           
           
            CommandBindings.Add(new CommandBinding(cmRemoveCalibrationViewset, new ExecutedRoutedEventHandler(cmRemoveCalibrationViewsetExecute)));
            CommandBindings.Add(new CommandBinding(cmResetCalibrationViewsets, new ExecutedRoutedEventHandler(cmResetCalibrationViewsetsExecute)));

            CommandBindings.Add(new CommandBinding(cmEstimateStereoCorrespondence, new ExecutedRoutedEventHandler(cmEstimateStereoCorrespondenceExecute)));
            CommandBindings.Add(new CommandBinding(cmEstimateAllStereoCorrespondences, new ExecutedRoutedEventHandler(cmEstimateAllStereoCorrespondencesExecute)));
            CommandBindings.Add(new CommandBinding(cmRemoveStereoCorrespondence, new ExecutedRoutedEventHandler(cmRemoveStereoCorrespondenceExecute)));

            CommandBindings.Add(new CommandBinding(cmRecalibrateCamera, new ExecutedRoutedEventHandler(cmRecalibrateCameraExecute)));

            CommandBindings.Add(new CommandBinding(cmManageCameras, new ExecutedRoutedEventHandler(cmManageCamerasExecute)));
            CommandBindings.Add(new CommandBinding(cmManageCamera, new ExecutedRoutedEventHandler(cmManageCameraExecute)));

            CommandBindings.Add(new CommandBinding(cmEstimate3DWorldTranslation, new ExecutedRoutedEventHandler(cmEstimate3DWorldTranslationExecute)));
            CommandBindings.Add(new CommandBinding(cmCaptureRulerOrigin,new ExecutedRoutedEventHandler(cmCaptureRulerOriginExecute)));
            
            InputBindings.Add(new InputBinding(cmTakeCalibrationViewset, new KeyGesture(Key.F5)));

            AddHandler(CommandManager.CanExecuteEvent, new CanExecuteRoutedEventHandler(CanExecuteAnyCommandHandler), true);
            AddHandler(CommandManager.PreviewExecutedEvent, new ExecutedRoutedEventHandler(PreviewCommandExecuted), true);
            AddHandler(CommandManager.ExecutedEvent, new ExecutedRoutedEventHandler(CommandExecuted), true);
            AddHandler(Hyperlink.RequestNavigateEvent, new System.Windows.Navigation.RequestNavigateEventHandler(HandleRequestNavigate));


            //Properties.Settings.Default.Reload();
            #region Load Application Settings

            DistortionWeight=(double) Properties.Settings.Default["DistortionWeight"];
            PoseWeight=(double) Properties.Settings.Default["PoseWeight"];
            BalanceWeight=(double) Properties.Settings.Default["BalanceWeight"];
            if (!string.IsNullOrEmpty(Properties.Settings.Default["LogFolder"].ToString()))
                LogFolder=Properties.Settings.Default["LogFolder"].ToString();
            if (!string.IsNullOrEmpty(Properties.Settings.Default["IRSensitivity"].ToString()))
                IRSensitivity=(int) Properties.Settings.Default["IRSensitivity"];
                else
                IRSensitivity=5; /// maximum

            LPTPortNumber =( int) Properties.Settings.Default["LPTPortNumber"];

            TriggerValue = (int)Properties.Settings.Default["TriggerValue"];
            RecordingTime = (int)Properties.Settings.Default["RecordingTime"];

            #endregion
            
            

            }

        private static void HandleRequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs args)
            {
            if (args.Uri != null)
                System.Diagnostics.Process.Start(args.Uri.ToString());
            }

        #region Application Command Management

        public static Task RunApplicationTask(Action myMethodName, bool RunAsynchroniusly)
            {
            Task CurrentTask = null;
            if (RunAsynchroniusly)
                {
                CurrentTask = Task.Factory.StartNew(() => myMethodName()); /// run the application task asynchronously
                }
            else
                {
                CurrentTask = new Task(() => myMethodName());
                CurrentTask.RunSynchronously();
                /// Run a dummy task that suits the synchronization logic
                }
            return CurrentTask;
            }

        private bool IsApplicationCommandExecuting = false;

        public bool _IsApplicationCommandExecuting
            {
            get { return IsApplicationCommandExecuting; }
            set
                {
                if (value == IsApplicationCommandExecuting) return;

                IsApplicationCommandExecuting = value;
                if (IsApplicationCommandExecuting == false)
                    CommandManager.InvalidateRequerySuggested();
                }
            }

        private void CanExecuteAnyCommandHandler(object sender, CanExecuteRoutedEventArgs e)
            {
            if (((e.Command as RoutedCommand).OwnerType != typeof(MainWindow)) &&
                ((e.Command as RoutedCommand).OwnerType != typeof(ApplicationCommands))) return; /// no locks for the UI commands

            if (_IsApplicationCommandExecuting)
                {
                e.CanExecute = false;
                }

            if (e.CanExecute)
                {
                }
            }

        private void PreviewCommandExecuted(object sender, ExecutedRoutedEventArgs e)
            {
            if (((e.Command as RoutedCommand).OwnerType == typeof(MainWindow)) || ((e.Command as RoutedCommand).OwnerType == typeof(ApplicationCommands)))
                _IsApplicationCommandExecuting = true;
            }

        private void CommandExecuted(object sender, ExecutedRoutedEventArgs e)
            {
            }

        #endregion Application Command Management

        private StringBuilder LogText = new StringBuilder();

        public string _LogText
            {
            get { return LogText.ToString(); }
            set
                {
                LogText = new StringBuilder(value);
                //OnPropertyChanged("_LogText");
                }
            }

        public int LogIndex = 0;

        public AsyncObservableCollection<LogEntry> _LogEntries { get; set; }

        public void WriteLogLine(string text, params object[] args)
            {
            _LogEntries.Add(new LogEntry()
                {
                    Index = LogIndex++,
                    DateTime = DateTime.Now,
                    Message = text,
                }
            );
            if (LogScrollViewer != null)
                LogScrollViewer.ScrollToBottom();
            }

        public void WriteErrorLogLine(string text, params object[] args)
            {
            System.Console.Beep(2000, 500);
            _LogEntries.Add(new LogEntry()
                {
                    Index = LogIndex++,
                    DateTime = DateTime.Now,
                    Message = text,
                }

            );
            if (LogScrollViewer != null)
                LogScrollViewer.ScrollToBottom();
            }

        private void EnumerateAvailableCameras()
            {
            AvailableWiimotes = new AsyncObservableCollection<CameraInfo>();
            WiimoteCollection Wiimotes = new WiimoteCollection();

            try
                {
                Wiimotes.FindAllCameras();
                }
            catch (Exception ex)
                {
                Wiimotes.Add(new Wiimote());
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message, "Wiimote not found error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            int n = 1;
            foreach (Wiimote W in Wiimotes)
                {
                string g=W.HIDDevicePath;
                string id="";

                if (g.IndexOf("pid")>=0)
                    id=g.Substring(g.IndexOf("pid")+11,7);
                    else
                    id="Wii"+n;
                AvailableWiimotes.Add(new CameraInfo(W, id));
                n++;
                }
            }




        private void ReenumerateAvailableCameras()
            {
            WiimoteCollection Cameras = new WiimoteCollection();
            try
                {
                Cameras.FindAllCameras();
                }
            catch //(Exception ex)
                {
                Cameras.Add(new Wiimote());
                //MessageBox.Show(ex.Message, "Wiimote not found error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            int n = 1;

            foreach (Wiimote W in Cameras)
                {   
                string g=W.HIDDevicePath;
                string id="";

                if (g.IndexOf("pid")>=0)
                    id=g.Substring(g.IndexOf("pid")+11,7);
                    else
                    id="Wii"+n;

                //string id=g.Substring(g.IndexOf("#9&")+3,8); 
                            
                foreach (CameraInfo WI in AvailableWiimotes)
                    {
                    if (WI._Index == id)
                        WI._Device = W;
                    }
                n++;
                }
            }

        private AsyncObservableCollection<CameraInfo> AvailableWiimotes;

        public AsyncObservableCollection<CameraInfo> _AvailableWiimotes
            {
            get { return AvailableWiimotes; }
            }

        private AsyncObservableCollection<CameraInfo> ActiveWiimotes = new AsyncObservableCollection<CameraInfo>();

        public AsyncObservableCollection<CameraInfo> _ActiveWiimotes
            {
            get
                {
                return ActiveWiimotes;
                }
            }

        private AsyncObservableCollection<CalibrationViewset> CalibrationSnapshots = new AsyncObservableCollection<CalibrationViewset>();

        public AsyncObservableCollection<CalibrationViewset> _CalibrationSnapshots
            {
            get { return CalibrationSnapshots; }
            set { CalibrationSnapshots = value; }
            }
        
        private AsyncObservableCollection<StereoCorrespondence> MeasuredStereoCorrespondences = new AsyncObservableCollection<StereoCorrespondence>();

        public AsyncObservableCollection<StereoCorrespondence> _MeasuredStereoCorrespondences
            {
            get { return MeasuredStereoCorrespondences; }
            }

        private void bStartCalibration_Click(object sender, RoutedEventArgs e)
            {
            List<CameraInfo> SelectedWiimotes = new List<CameraInfo>();

            foreach (CameraInfo WI in _ActiveWiimotes)
                {
                //if (Camera._IsIncludedInCalibration)
                SelectedWiimotes.Add(WI);
                }

            int CalibrationCount = 3;
            System.Drawing.Size imageSize = new System.Drawing.Size(1, 1);
            MCvPoint3D32f[][] objectPoints = new MCvPoint3D32f[CalibrationCount][];
            System.Drawing.PointF[][] imagePoints = new System.Drawing.PointF[CalibrationCount][];
            for (int i = 0; i < CalibrationCount; i++)
                {
                objectPoints[i] = CalibrationToken;
                imagePoints[i] = new System.Drawing.PointF[4];
                }

            foreach (CameraInfo WI in SelectedWiimotes)
                {
                for (int i = 0; i < CalibrationCount; i++)
                    {
                    for (int t=0;t<4;t++)
                        {
                        System.Drawing.PointF P = new System.Drawing.PointF();
                        P.X = (float)WI._TargetPositions3D[t].X;
                        P.Y = (float)WI._TargetPositions3D[t].Y;
                        imagePoints[i][t] = P;
                        }
                    System.Threading.Thread.Sleep(50);
                    }
                }

            foreach (CameraInfo WI in SelectedWiimotes)
                {
                IntrinsicCameraParameters IP = new IntrinsicCameraParameters();
                Emgu.CV.CvEnum.CALIB_TYPE CT = Emgu.CV.CvEnum.CALIB_TYPE.DEFAULT;
                ExtrinsicCameraParameters[] ECP;
                Emgu.CV.Structure.MCvTermCriteria TC = new MCvTermCriteria(10000);
                CameraCalibration.CalibrateCamera(objectPoints, imagePoints, imageSize, IP, CT, TC, out ECP);
                }
            }

        /// <summary>
        /// Wrapper for the OpenCV function UndistortPoints
        /// </summary>
        /// <param name="src1"></param>
        /// <param name="R1"></param>
        /// <param name="P1"></param>
        /// <param name="_intrinsicMatrix1"></param>
        /// <param name="_distortionCoeffs1"></param>
        /// <returns></returns>
        public PointF[] Undistort(PointF[] src, Matrix<double> R, Matrix<double> P, Matrix<double> _intrinsicMatrix,
                Matrix<double> _distortionCoeffs)
            {
            PointF[] dst = new PointF[src.Length];
            GCHandle srcHandle = GCHandle.Alloc(src, GCHandleType.Pinned);
            GCHandle dstHandle = GCHandle.Alloc(dst, GCHandleType.Pinned);
            using (Matrix<float> srcPointMatrix = new Matrix<float>(src.Length, 1, 2, srcHandle.AddrOfPinnedObject(), 2 * sizeof(float)))
            using (Matrix<float> dstPointMatrix = new Matrix<float>(dst.Length, 1, 2, dstHandle.AddrOfPinnedObject(), 2 * sizeof(float)))
                {
                CvInvoke.cvUndistortPoints(
                    srcPointMatrix, dstPointMatrix,
                    _intrinsicMatrix.Ptr,
                    _distortionCoeffs.Ptr,
                    R,
                    P);
                }
            srcHandle.Free();
            dstHandle.Free();
            return dst;
            }

        public double ComputeEpipolarCalibrationError(System.Drawing.PointF[][] src1, Matrix<double> R1, Matrix<double> P1, Matrix<double> _intrinsicMatrix1, Matrix<double> _distortionCoeffs1,
                    System.Drawing.PointF[][] src2, Matrix<double> R2, Matrix<double> P2, Matrix<double> _intrinsicMatrix2, Matrix<double> _distortionCoeffs2,

                 Matrix<double> Fundamental, List<CalibrationViewset> SelectedViewsets, CameraInfo WI1, CameraInfo WI2)
            {
            int N = src1.Length * src1[0].Length;

            MCvPoint3D32f[] lines1 = new MCvPoint3D32f[N];//(1,N);
            System.Drawing.PointF[] spoints1 = new System.Drawing.PointF[N];
            int n = 0;
            for (int i = 0; i < src1.Length; i++)
                {
                for (int l = 0; l < src1[0].Length; l++)
                    {
                    spoints1[n] = src1[i][l];
                    n++;
                    }
                }
            System.Drawing.PointF[] points1 = new System.Drawing.PointF[N];

            GCHandle srcHandle1 = GCHandle.Alloc(spoints1, GCHandleType.Pinned);
            GCHandle dstHandle1 = GCHandle.Alloc(points1, GCHandleType.Pinned);
            GCHandle lHandle1 = GCHandle.Alloc(lines1, GCHandleType.Pinned);

            using (Matrix<float> srcPointMatrix = new Matrix<float>(N, 1, 2, srcHandle1.AddrOfPinnedObject(), 2 * sizeof(float)))
            using (Matrix<float> dstPointMatrix = new Matrix<float>(N, 1, 2, dstHandle1.AddrOfPinnedObject(), 2 * sizeof(float)))
            using (Matrix<float> ll = new Matrix<float>(N, 1, 3, lHandle1.AddrOfPinnedObject(), 3 * sizeof(float)))
                {
                //CvInvoke.cvUndistortPoints(
                //    srcPointMatrix, dstPointMatrix,
                //    _intrinsicMatrix1.Ptr,
                //    _distortionCoeffs1.Ptr,
                //    R1,
                //    P1);
                CvInvoke.cvUndistortPoints(
                   srcPointMatrix, dstPointMatrix,
                   _intrinsicMatrix1.Ptr,
                   _distortionCoeffs1.Ptr,
                   IntPtr.Zero,
                   _intrinsicMatrix1.Ptr);// _intrinsicMatrix1.Ptr);
                CvInvoke.cvComputeCorrespondEpilines(dstPointMatrix, 1, Fundamental.Ptr, ll);
                }
            srcHandle1.Free();
            dstHandle1.Free();
            lHandle1.Free();

            MCvPoint3D32f[] lines2 = new MCvPoint3D32f[N];//(1,N);
            System.Drawing.PointF[] points2 = new System.Drawing.PointF[N];
            System.Drawing.PointF[] spoints2 = new System.Drawing.PointF[N];
            n = 0;
            for (int i = 0; i < src2.Length; i++)
                {
                for (int l = 0; l < src2[0].Length; l++)
                    {
                    spoints2[n] = src2[i][l];
                    n++;
                    }
                }

            GCHandle srcHandle2 = GCHandle.Alloc(spoints2, GCHandleType.Pinned);
            GCHandle dstHandle2 = GCHandle.Alloc(points2, GCHandleType.Pinned);
            GCHandle lHandle2 = GCHandle.Alloc(lines2, GCHandleType.Pinned);

            using (Matrix<float> srcPointMatrix = new Matrix<float>(N, 1, 2, srcHandle2.AddrOfPinnedObject(), 2 * sizeof(float)))
            using (Matrix<float> dstPointMatrix = new Matrix<float>(N, 1, 2, dstHandle2.AddrOfPinnedObject(), 2 * sizeof(float)))
            using (Matrix<float> l2 = new Matrix<float>(N, 1, 3, lHandle2.AddrOfPinnedObject(), 3 * sizeof(float)))
                {
                CvInvoke.cvUndistortPoints(
                srcPointMatrix, dstPointMatrix,
                _intrinsicMatrix2.Ptr,
                _distortionCoeffs2.Ptr,
                IntPtr.Zero,
                _intrinsicMatrix2.Ptr);
                CvInvoke.cvComputeCorrespondEpilines(dstPointMatrix, 2, Fundamental.Ptr, l2);
                }
            srcHandle2.Free();
            dstHandle2.Free();
            lHandle2.Free();

            double avgErr = 0;

            int ii = 0;
            for (int v = 0; v < src1.Length; v++)
                {
                double viewErr = 0;
                for (int l = 0; l < src1[0].Length; l++)
                    {
                    double err = Math.Abs(points1[ii].X * lines2[ii].x +
                        points1[ii].Y * lines2[ii].y + lines2[ii].z)
                        + Math.Abs(points2[ii].X * lines1[ii].x +
                        points2[ii].Y * lines1[ii].y + lines1[ii].z);

                    avgErr += err;
                    viewErr += err;
                    ii++;
                    }

                SelectedViewsets[v].AddError(WI1, WI2, viewErr / src1[0].Length);
                }

            return avgErr / N;
            }

       

        private void UpdateActiveWiimotes()
            {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                ActiveWiimotes.Clear();
                foreach (CameraInfo WI in AvailableWiimotes)
                    {
                    if (WI._IsConnected) ActiveWiimotes.Add(WI);
                    }
            })
                );
            }

        private ScrollViewer LogScrollViewer = null;

        private void icLogViewer_Loaded(object sender, RoutedEventArgs e)
            {
            LogScrollViewer = VisualTreeHelper.GetChild(icLogViewer, 0) as ScrollViewer;
            }

        

        private void cLogging_Click(object sender, RoutedEventArgs e)
            {
            if (cLogging.IsChecked == true)
                {
                cLogging.LargeIcon = "Resources\\Icons\\StopTracking.png";
                cLogging.Header = "Stop";
                WriteErrorLogLine("Logging is starting. Data will be automatically saved while tracking.");
                }
            else
                {
                cLogging.LargeIcon = "Resources\\Icons\\Start.png";
                cLogging.Header = "Start";
                //WriteErrorLogLine("Logging is starting");
                WriteErrorLogLine("Logging is stopped. No data will be saved while tracking.");
                }
            }

        private void SelectLogFolder_Click(object sender, RoutedEventArgs e)
            {
            try
                {
                using (FolderBrowserDialog dialog = new FolderBrowserDialog())
                    {
                    dialog.SelectedPath = LogFolder;
                    dialog.Description = "Select a folder in which the log files will be saved";
                    dialog.ShowNewFolderButton = false;
                    dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                        LogFolder = dialog.SelectedPath;
                        }
                    }
                }
            catch { }
            }

        

        private AsyncObservableCollection<ViewsetCaptor> CurrentViewsetCaptors = new AsyncObservableCollection<ViewsetCaptor>();

        public AsyncObservableCollection<ViewsetCaptor> _CurrentViewsetCaptors
            {
            get { return CurrentViewsetCaptors; }           
            }

        public int _CaptureInterval
            {
            get{return Convert.ToInt32(Properties.Settings.Default["CaptureInterval"]);}
            set{
                Properties.Settings.Default["CaptureInterval"]=value.ToString();
                OnPropertyChanged("_CaptureInterval");
                }
            }


        #region Ruler Origin
        private System.Windows.Media.Media3D.Point3D RulerOrigin;

        public System.Windows.Media.Media3D.Point3D _RulerOrigin
            {
            get { return RulerOrigin; }
            set { 
                RulerOrigin = value; 
                OnPropertyChanged("_RulerOrigin");
                OnPropertyChanged("_RulerOriginInfo");
                }
            }

        public string _RulerOriginInfo
            {
            get
                {                
                return "(" + RulerOrigin.X.ToString("0.00") + "," + RulerOrigin.Y.ToString("0.00") + "," + RulerOrigin.Z.ToString("0.00") + ")";                
                }
            }

        private void cmCaptureRulerOriginExecute(object sender, ExecutedRoutedEventArgs e)
            {
            if (b3DTracking.Header.ToString() != "Start 3D")
                {
                if (SelectedTargetForRuler>=0)
                    _RulerOrigin=_TrackedTargets[SelectedTargetForRuler]._TargetPosition;
                }
            _IsApplicationCommandExecuting = false;
            }

        double RulerDistance = 0;

        public string _RulerDistance
            {
            get {
            if (SelectedTargetForRuler<0) return "No target";
                return "Dist=" + Math.Sqrt(Math.Pow(RulerOrigin.X-_TrackedTargets[SelectedTargetForRuler]._TargetPosition.X,2)+
                            + Math.Pow(RulerOrigin.Y-_TrackedTargets[SelectedTargetForRuler]._TargetPosition.Y,2)+
                            + Math.Pow(RulerOrigin.Z-_TrackedTargets[SelectedTargetForRuler]._TargetPosition.Z,2)).ToString("#.00")+"";
                ;
                }
            }



        int SelectedTargetForRuler = 0;
        public int _SelectedTargetForRuler
            {
            get { 
                return SelectedTargetForRuler; 
                }
            set { 
                SelectedTargetForRuler = value; 
                }
            }

        
        #endregion

        }

   
    }